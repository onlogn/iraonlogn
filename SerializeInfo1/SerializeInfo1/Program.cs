﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace SerializeInfo1
{
    class Program
    {
        static void Main(string[] args)
        {
            string file = "testfile.txt";
            List<Book> bookList = new List<Book>();
            Book a = new Book(1, "Title 1", "Author 1", 2018);
            Book b = new Book(2, "Title 2", "Author 2", 2017);

            bookList.Add(a);
            bookList.Add(b);

            BinaryFormatter bf = new BinaryFormatter();

            using (FileStream fs = new FileStream(file, FileMode.OpenOrCreate))
            {
                bf.Serialize(fs,bookList);
            }

            List<Book> dsbook = new List<Book>();
            using (FileStream fs = new FileStream(file, FileMode.Open))
            {
                dsbook = (List<Book>)bf.Deserialize(fs);
            }

            foreach (var i in dsbook)
            {
                Console.WriteLine($"{i.Id}, {i.Author}, {i.Title}, {i.Year}");
            }
        }
    }
}
