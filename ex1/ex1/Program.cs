﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] data = new int[4,4];
            

            //string input = Console.ReadLine();
            //int[] srcArray = input.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries)
            //    .Select(int.Parse).ToArray();
            //int min = GetMinimum(srcArray);
            //int cnt = 0;
            //foreach (int v in srcArray)
            //{
            //    cnt += v == min ? 1 : 0;
            //}
            //Console.WriteLine(cnt);


            //var res = srcArray.Count(i => i == min);
            //Console.WriteLine(res);
        }

        static int GetMinimum(int[] a)
        {
            if(a.Length == 0) throw new Exception("Size of array = 0");
            int tmp = a[0];
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] < tmp)
                {
                    tmp = a[i];
                }
            }
            return tmp;
        }
    }
}
