﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerializeJSON
{
    class BookData
    {
        public int Id;
        public string Title;
        public string Author;
        public int Year;

        public BookData()
        {
            
        }
    }
}
