﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SerializeJSON
{
    class Program
    {
        static void Main(string[] args)
        {
            string file = "file.txt";
            List<Book> b = new List<Book>();
            b.Add(new Book(1,"1","11",11));
            b.Add(new Book(2,"12","121",121));

            string json = JsonConvert.SerializeObject(b);

            using (StreamWriter sw = new StreamWriter(file))
            {
                sw.Write(json);
            }

            string res;

            using (StreamReader sr = new StreamReader(file))
            {
                res = sr.ReadToEnd();
            }

            
            List<BookData> ans = JsonConvert.DeserializeObject<List<BookData>>(res);

            foreach (var v in ans)
            {
                Console.WriteLine($"{v.Id}, {v.Author}, {v.Title}, {v.Year}");
            }


        }
    }
}
