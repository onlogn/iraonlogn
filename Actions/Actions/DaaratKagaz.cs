﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actions
{
    public class DaaratKagaz
    {
        public string Name { get; set; }
        public int Weight { get; set; }
        public bool IsWight { get; set; }
        public bool IsStolen { get; set; }
        public bool IsLiquid { get; set; }
    }
}
