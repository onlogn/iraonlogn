﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actions
{
    public static class Service
    {
        public static void Show(this Gezit g)
        {
            Base(g, (x) => { Console.WriteLine($"Rigidity: {x.Rigidity}"); });
        }

        public static void Show(this Kitep g)
        {
            Base(g, (x) => { Console.WriteLine($"Author: {x.Author}"); });
        }

        private static void Base<T>(T g, Action<T> action) where T : DaaratKagaz
        {
            try
            {
                Console.WriteLine($"Name: {g.Name}");
                Console.WriteLine($"Weight: {g.Weight}");
                Console.WriteLine($"IsLiquid: {g.IsLiquid}");
                Console.WriteLine($"IsStolen: {g.IsStolen}");
                Console.WriteLine($"IsWight: {g.IsWight}");
                action(g);
            }
            catch (NotImplementedException)
            {
                Console.WriteLine("Haaaaaa");
            }
            finally
            {
                Console.WriteLine("Done");
            }
        }
    }
}
