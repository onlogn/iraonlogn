﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KRSUproblem25
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            Dictionary<string, int> d = new Dictionary<string, int>();
            string s;
            for (int i = 0; i < n; i++)
            {
                s = Console.ReadLine();
                if (!d.ContainsKey(s))
                {
                    d[s] = 1;
                }
                d[s]++;
            }

            int cnt = 10;
            foreach (var x in d.OrderByDescending(i => i.Value).ThenBy(i => i.Key))
            {
                if (cnt <= 0) break;
                cnt--;
                Console.WriteLine(x.Key);
            }

        }
    }
}
