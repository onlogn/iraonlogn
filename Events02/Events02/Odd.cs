﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events02
{
    public class Odd
    {
        public void Show(object sender, NumberEventArgs e)
        {
            Console.WriteLine($"Number {e.Number} is odd.");
        }
    }
}
