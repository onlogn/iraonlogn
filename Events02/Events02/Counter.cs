﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Events02
{
    public class Counter
    {
        public event EventHandler<NumberEventArgs> OddGenerated; 
        public event EventHandler<NumberEventArgs> EvenGenerated; 

        public void Count()
        {
            for (int i = 0; i < 100; i++)
            {
                NumberEventArgs tmp = new NumberEventArgs() {Number = i};
                if (i % 2 != 0)
                {
                    OnOddGenerated(tmp);
                }
                else
                {
                    OnEvenGenerated(tmp);
                }
                Thread.Sleep(500);
            }
        }

        protected virtual void OnOddGenerated(NumberEventArgs e)
        {
            OddGenerated?.Invoke(this, e);
        }

        protected virtual void OnEvenGenerated(NumberEventArgs e)
        {
            EvenGenerated?.Invoke(this, e);
        }
    }
}
