﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events02
{
    public class NumberEventArgs : EventArgs
    {
        public int Number { get; set; }
    }
}
