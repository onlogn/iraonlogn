﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events02
{
    class Program
    {
        static void Main(string[] args)
        {
            int numberOfHandlers = int.Parse(Console.ReadLine());

            Counter counter = new Counter();
            Odd odd = new Odd();
            Even even = new Even();

            counter.EvenGenerated += even.Show;
            counter.OddGenerated += odd.Show;

            counter.Count();
        }
    }
}
