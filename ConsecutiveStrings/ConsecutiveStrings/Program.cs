﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsecutiveStrings
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] strarr = { "zone", "abigail", "theta", "form", "libe", "zas", "theta", "abigail" };
            int m = 2;

            var strlenarr = strarr.Select(i => i.Length).ToArray();
            int max = 0;
            int idx = 0;
            for (int i = 0; i <= strlenarr.Length - m; i++)
            {
                var tmp = strlenarr.Skip(i).Take(m).Sum();
                if (tmp > max)
                {
                    max = tmp;
                    idx = i;
                }
            }

           Console.WriteLine(strarr.Skip(idx).Take(m).StrJoin());
        }

    }

    public static class StringExtension
    {
        public static string StrJoin(this IEnumerable<string> s)
        {
            StringBuilder str = new StringBuilder();
            foreach (var i in s)
            {
                str.Append(i);
            }

            return str.ToString();
        }
    }
}
