﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsPingPong
{
    class Program
    {
        static void Main(string[] args)
        {
            Ping ping = new Ping();
            Pong pong = new Pong();

            ping.PingEvent += pong.Show;
            pong.PongEvent += ping.Show;

            ping.Show(null,EventArgs.Empty);
        }
    }
}
