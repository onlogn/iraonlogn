﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EventsPingPong
{
    public class Ping
    {
        public event EventHandler<EventArgs> PingEvent; 

        public void Show(object sender, EventArgs e)
        {
            Console.WriteLine("Ping received Pong.");
            Thread.Sleep(200);
            OnPingEvent();
        }

        protected virtual void OnPingEvent()
        {
            PingEvent?.Invoke(this, EventArgs.Empty);
        }
    }
}
