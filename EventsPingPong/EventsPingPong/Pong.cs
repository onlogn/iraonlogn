﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EventsPingPong
{
    public class Pong
    {
        public event EventHandler<EventArgs> PongEvent; 

        public void Show(object sender, EventArgs e)
        {
            Console.WriteLine("Pong received Ping.");
            Thread.Sleep(200);
            OnPongEvent();
        }

        protected virtual void OnPongEvent()
        {
            PongEvent?.Invoke(this, EventArgs.Empty);
        }
    }
}
