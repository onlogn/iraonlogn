﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Events01
{
    public class Camera
    {
        public string Name { get; }

        public event EventHandler<CameraRegistredEventArgs> CameraRegisteredCrime;

        private Random _rnd = new Random();

        public Camera(string name)
        {
            Name = name;
        }

        public void MakeBadJob()
        {
            while (true)
            {
                int id = _rnd.Next(100);
                int s = _rnd.Next(1000);
                Console.WriteLine($"Driver id={id}, sum = {s}");
                OnCameraRegisteredCrime(new CameraRegistredEventArgs() { Id = id, Money = s });
                Thread.Sleep(2000);
            }
        }

        protected virtual void OnCameraRegisteredCrime(CameraRegistredEventArgs e)
        {
            CameraRegisteredCrime?.Invoke(this, e);
        }
    }
}
