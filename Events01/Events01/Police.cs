﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events01
{
    public class Police
    {
        public void ShowInfo(object sender, CameraRegistredEventArgs e)
        {
            Console.WriteLine($"Za toboi uje viehali! Driver {e.Id} za {e.Money}");
        }
    }
}
