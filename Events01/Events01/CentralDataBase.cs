﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events01
{
    public class CentralDataBase
    {
        private Random _rnd = new Random();
        public event EventHandler<CameraRegistredEventArgs> CrimeMade;
        public event EventHandler<CameraRegistredEventArgs> CrimeNotMade;


        public void CreateCrime(object sender, CameraRegistredEventArgs e)
        {
            string str = (sender as Camera).Name;
            Console.WriteLine($"Camera: {str}");

            int crime = _rnd.Next(2);
            if (crime == 0)
            {
                OnCrimeMade(e);
            }
            else
            {
                OnCrimeNotMade(e);
            }
        }

        protected virtual void OnCrimeMade(CameraRegistredEventArgs e)
        {
            CrimeMade?.Invoke(this, e);
        }

        protected virtual void OnCrimeNotMade(CameraRegistredEventArgs e)
        {
            CrimeNotMade?.Invoke(this, e);
        }
    }
}
