﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events01
{
    public class Newspaper
    {
        public void ShowInfo(object sender, CameraRegistredEventArgs e)
        {
            Console.WriteLine($"Congratulations! Driver {e.Id} paid {e.Money}! Loooooser");
        }
    }
}
