﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events01
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Camera> cam = new List<Camera>()
            {
                new Camera("Camera1"),
                new Camera("Camera2"),
                new Camera("Camera3")
            };

            CentralDataBase db = new CentralDataBase();
            foreach (Camera c in cam)
            {
                c.CameraRegisteredCrime += db.CreateCrime;
            }
           
            Police police = new Police();
            Newspaper news = new Newspaper();

            db.CrimeMade += police.ShowInfo;
            db.CrimeNotMade += news.ShowInfo;

            foreach (Camera c in cam)
            {
                Task.Run(()=>c.MakeBadJob());
                
            }

            Console.ReadKey();
        }
    }
}
