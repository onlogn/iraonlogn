﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OfficeSimulator
{
    public partial class ProgrammerControl : UserControl
    {
        private Programmer _current; 
        public ProgrammerControl(Programmer p)
        {
            InitializeComponent();
            _current = p;
            p.ProblemUpdated += POnProblemUpdated;
            lblSkillProger.Text = $"Skill: {p.Skill.ToString()}";
            groupBoxProgrammers.Text = $"Name: {p.Name}";
        }

        private void POnProblemUpdated(Problem obj)
        {
            progressBarProger.InvokeIfRequired(() =>
            {
                lblProblem.Text = $"Problem:#{obj.ProblemId} {obj.Name}";
                    progressBarProger.Value = Math.Min(100,
                        (int) ((double) obj.ProgressTimeToWork(_current) / obj.TimeToWork(_current) * 100));
                });
        }
    }
}
