﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace OfficeSimulator
{
    public partial class MainForm : Form
    {
        private BlockingCollection<Problem> _problemsFront = new BlockingCollection<Problem>();
        private BlockingCollection<Problem> _problemsBack = new BlockingCollection<Problem>();
        private int _problemCounter = 1;
        private const int DaySeconds = 5;

        private List<Programmer> _frontList = new List<Programmer>()
        {
            new Programmer("Vasya",SkillType.Junior, ProgrammerType.Frontend),
            new Programmer("Lesha",SkillType.Junior, ProgrammerType.Frontend),
            new Programmer("Petya",SkillType.Middle, ProgrammerType.Frontend),
            new Programmer("Masha",SkillType.Senior, ProgrammerType.Frontend)
        };

        private List<Programmer> _backList = new List<Programmer>()
        {
            new Programmer("Andrusha",SkillType.Junior, ProgrammerType.Backend),
            new Programmer("Erma",SkillType.Junior, ProgrammerType.Backend),
            new Programmer("Miran",SkillType.Middle, ProgrammerType.Backend),
            new Programmer("Bob",SkillType.Senior, ProgrammerType.Backend)
        };

        public MainForm()
        {
            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(0,0);
            InitializeComponent();
            
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            int numOfBackPr = _backList.Count;
            int numOfFrontPr = _frontList.Count;

            Task[] frontTasks = new Task[numOfFrontPr];
            Task[] backTasks = new Task[numOfBackPr];

            for (int i = 0; i < numOfFrontPr; i++)
            {
                int tmp = i;

                ProgrammerControl c = new ProgrammerControl(_frontList[tmp]);
                flpFrontProg.Controls.Add(c);
                frontTasks[i] = Task.Run(() => SolveProblem(_frontList[tmp]));  
            }

            for (int i = 0; i < numOfBackPr; i++)
            {
                int tmp = i;

                ProgrammerControl c = new ProgrammerControl(_backList[tmp]);
                flpBackProg.Controls.Add(c);
                backTasks[i] = Task.Run(() => SolveProblem(_backList[tmp]));
            }

        }

        private void AddProblem(Problem p)
        {
            _problemsFront.Add(p);
            _problemsBack.Add(p);
        }

        private void SolveProblem(Programmer user)
        {
            BlockingCollection<Problem> cur = null;
            if (user.ProgrammerType == ProgrammerType.Frontend)
            {
                cur = _problemsFront;
            }
            else if (user.ProgrammerType == ProgrammerType.Backend)
            {
                cur = _problemsBack;
            }
            foreach (var problem in cur.GetConsumingEnumerable())
            {
                int timeToWork = problem.TimeToWork(user);
                int progress = 0;
                while(true)
                {
                    progress += user.CodingModifier;
                    if (user.ProgrammerType == ProgrammerType.Frontend)
                    {
                        problem.ProgressFront = progress;
                    }
                    else if (user.ProgrammerType == ProgrammerType.Backend)
                    {
                        problem.ProgressBack = progress;
                    }
                    user.OnProblemUpdated(problem);
                    Thread.Sleep(DaySeconds * 1000);
                    if (progress > timeToWork)
                        break;
                }
            }
        }

        private void btnCreateProblem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtAddProblemName.Text)) return;
            Problem tmp = new Problem(_problemCounter++, txtAddProblemName.Text, (int)numFront.Value, (int)numBack.Value);
            flpProblems.Controls.Add(new ProblemControl(tmp));
            AddProblem(tmp);

            txtAddProblemName.Text = String.Empty;
            numFront.Value = 1;
            numBack.Value = 1;
        }

        
    }
}
