﻿namespace OfficeSimulator
{
    partial class ProblemControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxProblem = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblBackend = new System.Windows.Forms.Label();
            this.lblFront = new System.Windows.Forms.Label();
            this.progBarBackend = new System.Windows.Forms.ProgressBar();
            this.progBarFrontEnd = new System.Windows.Forms.ProgressBar();
            this.groupBoxProblem.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxProblem
            // 
            this.groupBoxProblem.Controls.Add(this.btnClose);
            this.groupBoxProblem.Controls.Add(this.lblBackend);
            this.groupBoxProblem.Controls.Add(this.lblFront);
            this.groupBoxProblem.Controls.Add(this.progBarBackend);
            this.groupBoxProblem.Controls.Add(this.progBarFrontEnd);
            this.groupBoxProblem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxProblem.Location = new System.Drawing.Point(0, 0);
            this.groupBoxProblem.Name = "groupBoxProblem";
            this.groupBoxProblem.Size = new System.Drawing.Size(242, 123);
            this.groupBoxProblem.TabIndex = 0;
            this.groupBoxProblem.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.Enabled = false;
            this.btnClose.Location = new System.Drawing.Point(156, 93);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Закрыть";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblBackend
            // 
            this.lblBackend.AutoSize = true;
            this.lblBackend.Location = new System.Drawing.Point(6, 52);
            this.lblBackend.Name = "lblBackend";
            this.lblBackend.Size = new System.Drawing.Size(32, 13);
            this.lblBackend.TabIndex = 3;
            this.lblBackend.Text = "Back";
            // 
            // lblFront
            // 
            this.lblFront.AutoSize = true;
            this.lblFront.Location = new System.Drawing.Point(6, 14);
            this.lblFront.Name = "lblFront";
            this.lblFront.Size = new System.Drawing.Size(31, 13);
            this.lblFront.TabIndex = 2;
            this.lblFront.Text = "Front";
            // 
            // progBarBackend
            // 
            this.progBarBackend.Location = new System.Drawing.Point(9, 67);
            this.progBarBackend.Name = "progBarBackend";
            this.progBarBackend.Size = new System.Drawing.Size(222, 20);
            this.progBarBackend.TabIndex = 1;
            // 
            // progBarFrontEnd
            // 
            this.progBarFrontEnd.Location = new System.Drawing.Point(9, 30);
            this.progBarFrontEnd.Name = "progBarFrontEnd";
            this.progBarFrontEnd.Size = new System.Drawing.Size(222, 20);
            this.progBarFrontEnd.TabIndex = 0;
            // 
            // ProblemControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxProblem);
            this.Name = "ProblemControl";
            this.Size = new System.Drawing.Size(242, 123);
            this.groupBoxProblem.ResumeLayout(false);
            this.groupBoxProblem.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxProblem;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblBackend;
        private System.Windows.Forms.Label lblFront;
        private System.Windows.Forms.ProgressBar progBarBackend;
        private System.Windows.Forms.ProgressBar progBarFrontEnd;
    }
}
