﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeSimulator
{
    public class Programmer
    {
        public string Name { get; private set; }
        public SkillType Skill { get; private set; }
        public ProgrammerType ProgrammerType { get; private set; }

        public event Action<Problem> ProblemUpdated;


        public int CodingModifier
        {
            get
            {
                switch (Skill)
                {
                    case SkillType.Junior:
                        return 1;
                    case SkillType.Middle:
                        return 4;
                    case SkillType.Senior:
                        return 12;
                    default:
                        throw new Exception("Invalid skill");
                }
            }
        } 

        public Programmer(string name, SkillType skill, ProgrammerType programmerType)
        {
            Name = name;
            Skill = skill;
            ProgrammerType = programmerType;
        }

        public virtual void OnProblemUpdated(Problem obj)
        {
            ProblemUpdated?.Invoke(obj);
        }
    }
}
