﻿namespace OfficeSimulator
{
    partial class ProgrammerControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxProgrammers = new System.Windows.Forms.GroupBox();
            this.lblSkillProger = new System.Windows.Forms.Label();
            this.progressBarProger = new System.Windows.Forms.ProgressBar();
            this.lblProblem = new System.Windows.Forms.Label();
            this.btnHoliday = new System.Windows.Forms.Button();
            this.groupBoxProgrammers.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxProgrammers
            // 
            this.groupBoxProgrammers.Controls.Add(this.btnHoliday);
            this.groupBoxProgrammers.Controls.Add(this.lblProblem);
            this.groupBoxProgrammers.Controls.Add(this.progressBarProger);
            this.groupBoxProgrammers.Controls.Add(this.lblSkillProger);
            this.groupBoxProgrammers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxProgrammers.Location = new System.Drawing.Point(0, 0);
            this.groupBoxProgrammers.Name = "groupBoxProgrammers";
            this.groupBoxProgrammers.Size = new System.Drawing.Size(240, 130);
            this.groupBoxProgrammers.TabIndex = 0;
            this.groupBoxProgrammers.TabStop = false;
            // 
            // lblSkillProger
            // 
            this.lblSkillProger.AutoSize = true;
            this.lblSkillProger.ForeColor = System.Drawing.Color.DarkMagenta;
            this.lblSkillProger.Location = new System.Drawing.Point(6, 28);
            this.lblSkillProger.Name = "lblSkillProger";
            this.lblSkillProger.Size = new System.Drawing.Size(26, 13);
            this.lblSkillProger.TabIndex = 0;
            this.lblSkillProger.Text = "Skill";
            // 
            // progressBarProger
            // 
            this.progressBarProger.Location = new System.Drawing.Point(6, 71);
            this.progressBarProger.Name = "progressBarProger";
            this.progressBarProger.Size = new System.Drawing.Size(228, 23);
            this.progressBarProger.TabIndex = 1;
            // 
            // lblProblem
            // 
            this.lblProblem.AutoSize = true;
            this.lblProblem.Location = new System.Drawing.Point(7, 55);
            this.lblProblem.Name = "lblProblem";
            this.lblProblem.Size = new System.Drawing.Size(48, 13);
            this.lblProblem.TabIndex = 2;
            this.lblProblem.Text = "Problem ";
            // 
            // btnHoliday
            // 
            this.btnHoliday.Location = new System.Drawing.Point(159, 101);
            this.btnHoliday.Name = "btnHoliday";
            this.btnHoliday.Size = new System.Drawing.Size(75, 23);
            this.btnHoliday.TabIndex = 3;
            this.btnHoliday.Text = "na kanikuly";
            this.btnHoliday.UseVisualStyleBackColor = true;
            // 
            // ProgrammerControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxProgrammers);
            this.Name = "ProgrammerControl";
            this.Size = new System.Drawing.Size(240, 130);
            this.groupBoxProgrammers.ResumeLayout(false);
            this.groupBoxProgrammers.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxProgrammers;
        private System.Windows.Forms.Button btnHoliday;
        private System.Windows.Forms.Label lblProblem;
        private System.Windows.Forms.ProgressBar progressBarProger;
        private System.Windows.Forms.Label lblSkillProger;
    }
}
