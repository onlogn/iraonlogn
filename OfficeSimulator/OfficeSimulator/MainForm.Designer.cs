﻿namespace OfficeSimulator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxMakeProblem = new System.Windows.Forms.GroupBox();
            this.btnCreateProblem = new System.Windows.Forms.Button();
            this.numBack = new System.Windows.Forms.NumericUpDown();
            this.numFront = new System.Windows.Forms.NumericUpDown();
            this.lblProblemName = new System.Windows.Forms.Label();
            this.txtAddProblemName = new System.Windows.Forms.TextBox();
            this.flpProblems = new System.Windows.Forms.FlowLayoutPanel();
            this.flpFrontProg = new System.Windows.Forms.FlowLayoutPanel();
            this.flpBackProg = new System.Windows.Forms.FlowLayoutPanel();
            this.lblMainProblems = new System.Windows.Forms.Label();
            this.lblMainBack = new System.Windows.Forms.Label();
            this.lblMainFront = new System.Windows.Forms.Label();
            this.groupBoxMakeProblem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFront)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxMakeProblem
            // 
            this.groupBoxMakeProblem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxMakeProblem.AutoSize = true;
            this.groupBoxMakeProblem.Controls.Add(this.btnCreateProblem);
            this.groupBoxMakeProblem.Controls.Add(this.numBack);
            this.groupBoxMakeProblem.Controls.Add(this.numFront);
            this.groupBoxMakeProblem.Controls.Add(this.lblProblemName);
            this.groupBoxMakeProblem.Controls.Add(this.txtAddProblemName);
            this.groupBoxMakeProblem.Location = new System.Drawing.Point(12, 538);
            this.groupBoxMakeProblem.Name = "groupBoxMakeProblem";
            this.groupBoxMakeProblem.Size = new System.Drawing.Size(814, 78);
            this.groupBoxMakeProblem.TabIndex = 0;
            this.groupBoxMakeProblem.TabStop = false;
            this.groupBoxMakeProblem.Text = "Создать задачу";
            // 
            // btnCreateProblem
            // 
            this.btnCreateProblem.Location = new System.Drawing.Point(635, 36);
            this.btnCreateProblem.Name = "btnCreateProblem";
            this.btnCreateProblem.Size = new System.Drawing.Size(173, 23);
            this.btnCreateProblem.TabIndex = 4;
            this.btnCreateProblem.Text = "Создать / добавить";
            this.btnCreateProblem.UseVisualStyleBackColor = true;
            this.btnCreateProblem.Click += new System.EventHandler(this.btnCreateProblem_Click);
            // 
            // numBack
            // 
            this.numBack.Location = new System.Drawing.Point(485, 38);
            this.numBack.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numBack.Name = "numBack";
            this.numBack.Size = new System.Drawing.Size(120, 20);
            this.numBack.TabIndex = 3;
            this.numBack.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numFront
            // 
            this.numFront.Location = new System.Drawing.Point(325, 38);
            this.numFront.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numFront.Name = "numFront";
            this.numFront.Size = new System.Drawing.Size(120, 20);
            this.numFront.TabIndex = 2;
            this.numFront.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblProblemName
            // 
            this.lblProblemName.AutoSize = true;
            this.lblProblemName.Location = new System.Drawing.Point(6, 19);
            this.lblProblemName.Name = "lblProblemName";
            this.lblProblemName.Size = new System.Drawing.Size(95, 13);
            this.lblProblemName.TabIndex = 1;
            this.lblProblemName.Text = "Название задачи";
            // 
            // txtAddProblemName
            // 
            this.txtAddProblemName.Location = new System.Drawing.Point(6, 38);
            this.txtAddProblemName.Name = "txtAddProblemName";
            this.txtAddProblemName.Size = new System.Drawing.Size(283, 20);
            this.txtAddProblemName.TabIndex = 0;
            // 
            // flpProblems
            // 
            this.flpProblems.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpProblems.AutoScroll = true;
            this.flpProblems.Location = new System.Drawing.Point(12, 28);
            this.flpProblems.Name = "flpProblems";
            this.flpProblems.Size = new System.Drawing.Size(808, 128);
            this.flpProblems.TabIndex = 1;
            // 
            // flpFrontProg
            // 
            this.flpFrontProg.AutoScroll = true;
            this.flpFrontProg.Location = new System.Drawing.Point(12, 181);
            this.flpFrontProg.Name = "flpFrontProg";
            this.flpFrontProg.Size = new System.Drawing.Size(808, 155);
            this.flpFrontProg.TabIndex = 2;
            this.flpFrontProg.WrapContents = false;
            // 
            // flpBackProg
            // 
            this.flpBackProg.AutoScroll = true;
            this.flpBackProg.Location = new System.Drawing.Point(12, 363);
            this.flpBackProg.Name = "flpBackProg";
            this.flpBackProg.Size = new System.Drawing.Size(808, 169);
            this.flpBackProg.TabIndex = 3;
            this.flpBackProg.WrapContents = false;
            // 
            // lblMainProblems
            // 
            this.lblMainProblems.AutoSize = true;
            this.lblMainProblems.Location = new System.Drawing.Point(12, 9);
            this.lblMainProblems.Name = "lblMainProblems";
            this.lblMainProblems.Size = new System.Drawing.Size(43, 13);
            this.lblMainProblems.TabIndex = 4;
            this.lblMainProblems.Text = "Задачи";
            // 
            // lblMainBack
            // 
            this.lblMainBack.AutoSize = true;
            this.lblMainBack.Location = new System.Drawing.Point(12, 160);
            this.lblMainBack.Name = "lblMainBack";
            this.lblMainBack.Size = new System.Drawing.Size(71, 13);
            this.lblMainBack.TabIndex = 5;
            this.lblMainBack.Text = "Бэкендщики";
            // 
            // lblMainFront
            // 
            this.lblMainFront.AutoSize = true;
            this.lblMainFront.Location = new System.Drawing.Point(12, 342);
            this.lblMainFront.Name = "lblMainFront";
            this.lblMainFront.Size = new System.Drawing.Size(86, 13);
            this.lblMainFront.TabIndex = 6;
            this.lblMainFront.Text = "Фронтендщики";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(832, 618);
            this.Controls.Add(this.lblMainFront);
            this.Controls.Add(this.lblMainBack);
            this.Controls.Add(this.lblMainProblems);
            this.Controls.Add(this.flpBackProg);
            this.Controls.Add(this.flpFrontProg);
            this.Controls.Add(this.flpProblems);
            this.Controls.Add(this.groupBoxMakeProblem);
            this.Name = "MainForm";
            this.Text = "Симулятор офиса программистов";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBoxMakeProblem.ResumeLayout(false);
            this.groupBoxMakeProblem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFront)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxMakeProblem;
        private System.Windows.Forms.Button btnCreateProblem;
        private System.Windows.Forms.NumericUpDown numBack;
        private System.Windows.Forms.NumericUpDown numFront;
        private System.Windows.Forms.Label lblProblemName;
        private System.Windows.Forms.TextBox txtAddProblemName;
        private System.Windows.Forms.FlowLayoutPanel flpProblems;
        private System.Windows.Forms.FlowLayoutPanel flpFrontProg;
        private System.Windows.Forms.FlowLayoutPanel flpBackProg;
        private System.Windows.Forms.Label lblMainProblems;
        private System.Windows.Forms.Label lblMainBack;
        private System.Windows.Forms.Label lblMainFront;
    }
}

