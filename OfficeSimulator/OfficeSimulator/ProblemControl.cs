﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OfficeSimulator
{
    public partial class ProblemControl : UserControl
    {
        private Problem _currentProblem;
        public int CurProblemId => _currentProblem.ProblemId;

        public ProblemControl(Problem p)
        {
            InitializeComponent();
            _currentProblem = p;
            p.FrontProgressChanged += POnFrontProgressChanged;
            p.BackProgressChanged += POnBackProgressChanged;
            ShowInformation();
        }

        private void POnBackProgressChanged(int obj)
        {
            progBarFrontEnd.InvokeIfRequired(() =>
            {
                progBarBackend.Value = Math.Min(100,(int)((double)obj / _currentProblem.TimeToBackend * 100));
                ButtonCheck();
            });
        }

        private void POnFrontProgressChanged(int progress)
        {
            progBarFrontEnd.InvokeIfRequired(() =>
                {
                    progBarFrontEnd.Value = Math.Min(100,(int)((double) progress / _currentProblem.TimeToFrontend * 100));
                    ButtonCheck();
                });
        }

        private void ButtonCheck()
        {
            btnClose.Enabled = _currentProblem.IsComplete();
        }

        public void ShowInformation()
        {
            this.InvokeIfRequired(() =>
            {
                groupBoxProblem.Text = $"Задача #{_currentProblem.ProblemId}: {_currentProblem.Name}";
                lblFront.Text = "Frontend: ";
                lblBackend.Text = "Backend ";
            });
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }
    }
}
