﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeSimulator
{
    public class Problem
    {
        private int _progressBack;
        private int _progressFront;
        public string Name { get; private set; }
        public int TimeToFrontend { get; private set; }
        public int TimeToBackend { get; private set; }
        public int ProblemId { get; private set; }

        public int ProgressFront
        {
            get => _progressFront;
            set
            {
                _progressFront = value;
                OnFrontProgressChanged(_progressFront);
            }
        }

        public int ProgressBack
        {
            get => _progressBack;
            set
            {
                _progressBack = value;
                OnBackProgressChanged(_progressBack);
            }
        }

        public event Action<int> FrontProgressChanged;
        public event Action<int> BackProgressChanged;

        public Problem(int problemId, string name, int timeToFrontend, int timeToBackend)
        {
            Name = name;
            TimeToFrontend = timeToFrontend;
            TimeToBackend = timeToBackend;
            ProblemId = problemId;
        }

        public int TimeToWork(Programmer p)
        {
            return p.ProgrammerType == ProgrammerType.Frontend ? TimeToFrontend : TimeToBackend;
        }
        public int ProgressTimeToWork(Programmer p)
        {
            return p.ProgrammerType == ProgrammerType.Frontend ? ProgressFront : ProgressBack;
        }

        public bool IsComplete()
        {
            return ProgressFront >= TimeToFrontend && ProgressBack >= TimeToBackend;
        }

        protected virtual void OnFrontProgressChanged(int obj)
        {
            FrontProgressChanged?.Invoke(obj);
        }

        protected virtual void OnBackProgressChanged(int obj)
        {
            BackProgressChanged?.Invoke(obj);
        }
    }
}
