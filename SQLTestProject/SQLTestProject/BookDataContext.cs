﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLTestProject
{
    class BookDataContext : DbContext
    {
        public BookDataContext() : base("DBCon")
        {
            
        }

        public DbSet<BookData> BookDatas { get; set; }
    }
}
