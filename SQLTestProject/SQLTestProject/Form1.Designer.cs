﻿namespace SQLTestProject
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnShowDBTable = new System.Windows.Forms.Button();
            this.dgvBooks = new System.Windows.Forms.DataGridView();
            this.btnAddBook = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.changeDataBaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuADO = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileDB = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuEntityDB = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBooks)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnShowDBTable
            // 
            this.btnShowDBTable.Location = new System.Drawing.Point(12, 35);
            this.btnShowDBTable.Name = "btnShowDBTable";
            this.btnShowDBTable.Size = new System.Drawing.Size(144, 53);
            this.btnShowDBTable.TabIndex = 0;
            this.btnShowDBTable.Text = "Show Database Table";
            this.btnShowDBTable.UseVisualStyleBackColor = true;
            this.btnShowDBTable.Click += new System.EventHandler(this.btnShowDBTable_Click);
            // 
            // dgvBooks
            // 
            this.dgvBooks.AllowUserToAddRows = false;
            this.dgvBooks.AllowUserToDeleteRows = false;
            this.dgvBooks.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvBooks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBooks.Location = new System.Drawing.Point(12, 94);
            this.dgvBooks.MultiSelect = false;
            this.dgvBooks.Name = "dgvBooks";
            this.dgvBooks.ReadOnly = true;
            this.dgvBooks.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBooks.Size = new System.Drawing.Size(547, 344);
            this.dgvBooks.TabIndex = 1;
            // 
            // btnAddBook
            // 
            this.btnAddBook.Location = new System.Drawing.Point(283, 35);
            this.btnAddBook.Name = "btnAddBook";
            this.btnAddBook.Size = new System.Drawing.Size(81, 53);
            this.btnAddBook.TabIndex = 2;
            this.btnAddBook.Text = "Add";
            this.btnAddBook.UseVisualStyleBackColor = true;
            this.btnAddBook.Click += new System.EventHandler(this.btnAddBook_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(370, 35);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(79, 53);
            this.btnEdit.TabIndex = 3;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(455, 35);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(75, 53);
            this.btnRemove.TabIndex = 4;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changeDataBaseToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(569, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // changeDataBaseToolStripMenuItem
            // 
            this.changeDataBaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuADO,
            this.mnuFileDB,
            this.mnuEntityDB});
            this.changeDataBaseToolStripMenuItem.Name = "changeDataBaseToolStripMenuItem";
            this.changeDataBaseToolStripMenuItem.Size = new System.Drawing.Size(111, 20);
            this.changeDataBaseToolStripMenuItem.Text = "Change DataBase";
            // 
            // mnuADO
            // 
            this.mnuADO.Checked = true;
            this.mnuADO.CheckOnClick = true;
            this.mnuADO.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mnuADO.Name = "mnuADO";
            this.mnuADO.Size = new System.Drawing.Size(214, 22);
            this.mnuADO.Text = "ADO DataBase";
            this.mnuADO.Click += new System.EventHandler(this.MenuItemClick);
            // 
            // mnuFileDB
            // 
            this.mnuFileDB.CheckOnClick = true;
            this.mnuFileDB.Name = "mnuFileDB";
            this.mnuFileDB.Size = new System.Drawing.Size(214, 22);
            this.mnuFileDB.Text = "File DataBase";
            this.mnuFileDB.Click += new System.EventHandler(this.MenuItemClick);
            // 
            // mnuEntityDB
            // 
            this.mnuEntityDB.CheckOnClick = true;
            this.mnuEntityDB.Name = "mnuEntityDB";
            this.mnuEntityDB.Size = new System.Drawing.Size(214, 22);
            this.mnuEntityDB.Text = "EntityFramework DataBase";
            this.mnuEntityDB.Click += new System.EventHandler(this.MenuItemClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(569, 442);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnAddBook);
            this.Controls.Add(this.dgvBooks);
            this.Controls.Add(this.btnShowDBTable);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBooks)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnShowDBTable;
        private System.Windows.Forms.DataGridView dgvBooks;
        private System.Windows.Forms.Button btnAddBook;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem changeDataBaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuADO;
        private System.Windows.Forms.ToolStripMenuItem mnuFileDB;
        private System.Windows.Forms.ToolStripMenuItem mnuEntityDB;
    }
}

