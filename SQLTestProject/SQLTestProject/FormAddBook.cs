﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQLTestProject
{
    public partial class FormAddBook : Form
    {

        public Book ResBook { get; private set; }

        public FormAddBook()
        {
            InitializeComponent();
        }

        public FormAddBook(Book a):this()
        {
            ResBook = new Book(a);
            txtTitle.Text = a.Title;
            txtAuthor.Text = a.Author;
            numYearAdd.Value = a.Year;
        }

        private void btnAddCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //int idx = ResBook != null ? ResBook.Id : -1;
            //int? idkkx = ResBook != null ? ResBook.Id : (int?)null;
            int idx = ResBook?.Id ?? -1;
            ResBook = new Book(idx,txtTitle.Text,txtAuthor.Text,(int)numYearAdd.Value);

            DialogResult = DialogResult.OK;
        }


    }
}
