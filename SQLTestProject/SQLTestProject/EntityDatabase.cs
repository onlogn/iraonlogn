﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLTestProject
{
    class EntityDatabase : IDataProvider
    {

        public EntityDatabase()
        {
            
        }

        public int AddBookInDB(Book a)
        {
            int idx = -1;
            using (BookDataContext bk = new BookDataContext())
            {
                BookData tmp = new BookData(a);
                bk.BookDatas.Add(tmp);
                bk.SaveChanges();

                idx = tmp.Id;
            }

            return idx;
        }

        public void UpdateBookInDB(Book a)
        {
            using (BookDataContext bk = new BookDataContext())
            {
                var res = bk.BookDatas.SingleOrDefault(i => i.Id == a.Id);
                if (res != null)
                {
                    res.Author = a.Author;
                    res.Title = a.Title;
                    res.Year = a.Year;

                    bk.SaveChanges();
                }
            }
        }

        public void DeleteBookFromDB(Book a)
        {
            using (BookDataContext bk = new BookDataContext())
            {
                BookData res = bk.BookDatas.Find(a.Id);
                if (res == null) return;
                bk.BookDatas.Remove(res);
                bk.SaveChanges();
            }

        }

        public List<Book> GetBooksList()
        {
            
            List<BookData> tmp = new List<BookData>();
            using (BookDataContext bk = new BookDataContext())
            {
                bk.BookDatas.Load();
                tmp = bk.BookDatas.Local.ToList();
            }

            return tmp.Select(i => new Book(i.Id, i.Title, i.Author, i.Year)).ToList();
        }
    }
}
