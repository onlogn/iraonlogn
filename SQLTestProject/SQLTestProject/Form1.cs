﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQLTestProject
{
    public partial class Form1 : Form
    {
        private BindingList<Book> bl;
        private IDataProvider db = new ADODatabase();

        public Form1()
        {
            InitializeComponent();
        }

        private void btnShowDBTable_Click(object sender, EventArgs e)
        {
            bl = new BindingList<Book>(db.GetBooksList());
            ShowItemsInDatagreed(bl);
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void btnAddBook_Click(object sender, EventArgs e)
        {
            FormAddBook ab = new FormAddBook();

            if (ab.ShowDialog(this) == DialogResult.OK)
            {
                int idx = db.AddBookInDB(ab.ResBook);
                bl.Add(new Book(idx, ab.ResBook.Title, ab.ResBook.Author, ab.ResBook.Year));
                //ShowItemsInDatagreed(bl); это прикол binding list - это писать не надо!!!!
            }
        }

        

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgvBooks.SelectedRows.Count == 0)
                return;

            Book bookToEdit = dgvBooks.SelectedRows[0].DataBoundItem as Book; // (Book)dgvBooks.SelectedRows[0].DataBoundItem

            FormAddBook ab = new FormAddBook(bookToEdit);
            
            if (ab.ShowDialog(this) == DialogResult.OK)
            {
                db.UpdateBookInDB(ab.ResBook);
                bl[bl.IndexOf(bookToEdit)] = new Book(ab.ResBook);
                //ShowItemsInDatagreed(bl);
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (dgvBooks.SelectedRows.Count == 0)
                return;

            Book bookToRemove = dgvBooks.SelectedRows[0].DataBoundItem as Book;

            db.DeleteBookFromDB(bookToRemove);
            bl.Remove(bookToRemove);
            //ShowItemsInDatagreed(bl);
        }

        private void MenuItemClick(object sender, EventArgs e)
        {
            ToolStripMenuItem s = sender as ToolStripMenuItem;
            if (!s.Checked)
            {
                s.Checked = true;
                return;
            }
            if (mnuADO.Checked && s == mnuADO)
            {
                mnuEntityDB.Checked = false;
                mnuFileDB.Checked = false;
                db = new ADODatabase();
            }
            else if (mnuEntityDB.Checked && s == mnuEntityDB)
            {
                mnuADO.Checked = false;
                mnuFileDB.Checked = false;
                db = new EntityDatabase();
            }
            else if (mnuFileDB.Checked && s == mnuFileDB)
            {
                mnuADO.Checked = false;
                mnuEntityDB.Checked = false;
                db = new FileDataBase();
            }

            
            bl = new BindingList<Book>(db.GetBooksList());
            ShowItemsInDatagreed(bl);
        }

        private void ShowItemsInDatagreed(BindingList<Book> bl)
        {
            dgvBooks.DataSource = bl;
        }
    }
}
