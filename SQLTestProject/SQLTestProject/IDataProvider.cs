﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLTestProject
{
    interface IDataProvider
    {
        int AddBookInDB(Book a);
        void UpdateBookInDB(Book a);
        void DeleteBookFromDB(Book a);
        List<Book> GetBooksList();
    }
}
