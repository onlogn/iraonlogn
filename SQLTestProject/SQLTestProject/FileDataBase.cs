﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SQLTestProject
{
    class FileDataBase: IDataProvider
    {
        private string _file = @"db.txt";
        
        public int AddBookInDB(Book a)
        {
            List<Book> books = GetBooksList();
            int prevId = (books.Count == 0) ? 0 : books[books.Count - 1].Id;

            Book tmp = new Book(prevId + 1, a.Title, a.Author, a.Year);
            books.Add(tmp);

            WriteFile(ToBookDataList(books));

            return tmp.Id;
        }

        public void UpdateBookInDB(Book a)
        {
            List<Book> books = GetBooksList();

            int ix = -1;

            for (int i = 0; i < books.Count; i++)
            {
                if (books[i].Id == a.Id)
                {
                    ix = i;
                    break;
                }
            }

            books[ix] = new Book(a.Id,a.Title,a.Author,a.Year);

            WriteFile(ToBookDataList(books));
        }

        public void DeleteBookFromDB(Book a)
        {
            List<Book> books = GetBooksList();
            
            books.Remove(a);

            WriteFile(ToBookDataList(books));
        }

        public List<Book> GetBooksList()
        {
            List<BookData> tmp = ReadFile();

            List<Book> res = new List<Book>();
            if (tmp != null)
            {
                foreach (BookData i in tmp)
                {
                    res.Add(new Book(i.Id, i.Title, i.Author, i.Year));
                }
            }

            return res;
        }

        private void WriteFile(List<BookData> b)
        {
            string serializedJson = JsonConvert.SerializeObject(b); 
            using (StreamWriter sw = new StreamWriter(_file, false, System.Text.Encoding.Default))
            {
                sw.Write(serializedJson);
            }
        }

        private List<BookData> ReadFile()
        {
            string res;

            using (StreamReader sr = new StreamReader(_file))
            {
                res = sr.ReadToEnd();
            }

            return JsonConvert.DeserializeObject<List<BookData>>(res);
        }

        private List<BookData> ToBookDataList(List<Book> b)
        {
            return b ?.Select(i => new BookData(i)).ToList() ?? new List<BookData>();
        }
    }
}
