﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLTestProject
{
    public class ADODatabase : IDataProvider
    {
        private static string connectionString = @"Data Source=.;Initial Catalog=LibraryBooks;Integrated Security=True";

        public ADODatabase()
        {
        }

        public int AddBookInDB(Book b)
        {
            string query = "INSERT INTO Books VALUES(@title,@author,@year)";
            int idx = -1;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);

                SqlParameter titleParam = new SqlParameter("@title", b.Title);
                command.Parameters.Add(titleParam);

                SqlParameter authorParam = new SqlParameter("@author", b.Author);
                command.Parameters.Add(authorParam);

                SqlParameter yearParam = new SqlParameter("@year", b.Year);
                command.Parameters.Add(yearParam);

                command.ExecuteNonQuery();

                query = "SELECT TOP 1 Id From Books ORDER BY Id DESC";
                command = new SqlCommand(query,connection);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        idx = reader.GetInt32(0);
                    }
                }

                reader.Close();

            }

            return idx;
        }

        public void UpdateBookInDB(Book b)
        {
            string query = "UPDATE Books SET Title = @title, Author = @author, Year = @year WHERE Id = @id";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);

                SqlParameter titleParam = new SqlParameter("@title", b.Title);
                command.Parameters.Add(titleParam);

                SqlParameter authorParam = new SqlParameter("@author", b.Author);
                command.Parameters.Add(authorParam);

                SqlParameter yearParam = new SqlParameter("@year", b.Year);
                command.Parameters.Add(yearParam);

                SqlParameter idParam = new SqlParameter("@id", b.Id);
                command.Parameters.Add(idParam);

                command.ExecuteNonQuery();
            }
        }

        public void DeleteBookFromDB(Book b)
        {
            string query = "DELETE FROM Books WHERE Id = @id";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);

                SqlParameter idParam = new SqlParameter("@id", b.Id);
                command.Parameters.Add(idParam);

                command.ExecuteNonQuery();
            }
        }

        public List<Book> GetBooksList()
        {
            List<Book> bookList = new List<Book>();

            string sqlExpression = "SELECT Id, Title, Author, Year FROM Books";

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                SqlCommand command = new SqlCommand(sqlExpression, con);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        bookList.Add(new Book(reader.GetInt32(0), reader.GetString(1),
                            reader.GetString(2), reader.GetInt32(3)));
                    }
                }

                reader.Close();
            }

            return bookList;
        }
    }
}