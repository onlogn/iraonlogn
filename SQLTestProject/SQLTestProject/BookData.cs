﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLTestProject
{
    [Table("Books")]
    class BookData
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public int Year { get; set; }

        public BookData()
        {
            
        }

        public BookData(Book a)
        {
            Id = a.Id;
            Title = a.Title;
            Author = a.Author;
            Year = a.Year;
        }
    }
}
