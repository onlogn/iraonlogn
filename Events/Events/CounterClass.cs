﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Events
{
    public class CounterClass
    {
        public event Action Event70;
        public event Action Event71;

        public void Count()
        {
            for (int i = 0; i < 100; i++)
            {
                if (i == 70)
                {
                    OnEvent70();
                }

                if (i == 71)
                {
                    OnEvent71();
                }

                Thread.Sleep(100);
                Console.WriteLine(i);
            }
        }

        // скажет, что событие Event70 произошло, и все кто подписался должны отреагировать на него
        protected virtual void OnEvent70()
        {
            //if (Event70!=null) Event70();
            Event70?.Invoke();
        }

        // скажет, что событие Event71 произошло, и все кто подписался должны отреагировать на него
        protected virtual void OnEvent71()
        {
            Event71?.Invoke();
        }
    }
}
