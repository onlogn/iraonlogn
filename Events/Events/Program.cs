﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events
{
    class Program
    {
        static void Main(string[] args)
        {

            CounterClass c = new CounterClass();
            Handler1 h1 = new Handler1();
            Handler2 h2 = new Handler2();

            c.Event70 += ()=>
            {
                h1.Show(5);
            };
            c.Event71 += h2.Ololo;

            c.Count();
        }
    }
}
