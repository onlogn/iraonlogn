﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YProtocol
{
    public class FileInfoPacket
    {
        public int FileSize { get; set; }
        public string FileName { get; set; }

        public FileInfoPacket(byte[] bytes)
        {
            
        }

        public FileInfoPacket(int fileSize, string fileName)
        {
            
        }

        public byte[] GetBytes()
        {

        } 
    }
}
