﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Sockets
{
    class Program
    {
        private static readonly int port = 13221;
        static void Main(string[] args)
        {
            IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);
            Socket listenSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                listenSocket.Bind(ipPoint);
                listenSocket.Listen(10);

                Console.WriteLine("Waiting for connections...");

                FileReceiver receiver = new FileReceiver(5 * 1024 * 1024);
                while (true)
                {
                    Socket acceptSocket = listenSocket.Accept();

                    receiver.ReceiveFile(acceptSocket);

                    acceptSocket.Shutdown(SocketShutdown.Both);
                    acceptSocket.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
