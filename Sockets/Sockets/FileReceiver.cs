﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using YProtocol;

namespace Sockets
{
    public class FileReceiver
    {
        public int MaxFileSize { get; }

        public FileReceiver(int maxFileSize)
        {
            MaxFileSize = maxFileSize;
        }

        public FileInfo ReceiveFile(Socket socket)
        {
            byte[] fileInfoPacketBytes = new byte[256];
            socket.Receive(fileInfoPacketBytes);
            FileInfoPacket filePacket = new FileInfoPacket(fileInfoPacketBytes);

            if (filePacket.FileSize > MaxFileSize)
            {
                socket.Send(new byte[] { 1 });
                return null;
            }
            else
            {

                socket.Send(new byte[] { 0 });
                byte[] data = new byte[256];
                try
                {
                    using (BinaryWriter writer = new BinaryWriter(File.Open(filePacket.FileName, FileMode.OpenOrCreate)))
                    {
                        do
                        {
                            int bytes = socket.Receive(data);
                            writer.Write(data.Take(bytes).ToArray());

                        } while (socket.Available > 0);
                    }

                    socket.Send(new byte[] { 0 });

                    return new FileInfo(filePacket.FileName);
                }
                catch (Exception ex)
                {
                    socket.Send(new byte[] { 1 });
                    Console.WriteLine(ex.Message);
                    return null;
                }

            }
        }
    }
}
