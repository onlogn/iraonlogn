﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace regexSplit
{
    class Program
    {
        static void Main(string[] args)
        {
            string m = "<ta>bLe<TaBlewidth=100></table></ta>";
            Regex r = new Regex("<.*?>",RegexOptions.IgnoreCase);

            MatchCollection matches = r.Matches(m);

            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                    Console.WriteLine(match.Index);
            }
            else
            {
                Console.WriteLine("Совпадений не найдено");
            }
        }
    }
}
