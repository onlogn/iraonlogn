﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace StolovkaTask
{
    public partial class MainForm : Form
    {
        private BlockingCollection<FoodOrder> _foodOrders = new BlockingCollection<FoodOrder>();
        private BlockingCollection<FoodOrder> _drinkOrders = new BlockingCollection<FoodOrder>();

        private List<string> _foodMenu = new List<string>() { "Мясное блюдо", "Пицца", "Салатик", "Пирожок", "Тортик", "Пироженка", "Мороженка" };
        private List<string> _drinkMenu = new List<string>() {"Чай", "Сок", "Кофе", "Пиво", "Коктейль", "Крепкий алкогольный напиток"};

        private Random _rnd = new Random();

        private int _currentTime = 0;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            int numOfTables = 5;
            int numOfCoockers = 3;
            int numOfBarmens = 2;

            Task[] tablesTasks = new Task[numOfTables];
            for (int i = 0; i < numOfTables; i++)
            {
                int id = i;
                tablesTasks[i] = Task.Factory.StartNew(()=>DoOrder(id));
            }

            Thread.Sleep(100);

           
            Task[] CoockerTasks = new Task[numOfCoockers];
            for (int i = 0; i < numOfCoockers; i++)
            {
                int id = i;
                Info control = new Info();
                flp.Controls.Add(control);
                CoockerTasks[i] = Task.Factory.StartNew(()=>FinishFoodOrder(control, id));
            }

            
            Task[] barmenTasks = new Task[numOfBarmens];
            for (int i = 0; i < numOfBarmens; i++)
            {
                int id = i;
                Info control = new Info();
                flp.Controls.Add(control);
                barmenTasks[i] = Task.Factory.StartNew(() => FinishDrinkOrder(control, id));
            }

            timer.Start();
        }
        //Producer
        private void DoOrder(int tableId)
        {
            while (true)
            {
                FoodType t = (FoodType) _rnd.Next(Enum.GetNames(typeof(FoodType)).Length);
                int preparingTime = _rnd.Next(1, 20);
                FoodOrder tmp;

                if (t == FoodType.Juice)
                {
                    string foodName = _drinkMenu[_rnd.Next(_foodMenu.Count)];
                    tmp = new FoodOrder(foodName, preparingTime, t, tableId);
                    _drinkOrders.Add(tmp);
                }
                else
                {
                    string foodName = _foodMenu[_rnd.Next(_foodMenu.Count)];
                    tmp = new FoodOrder(foodName, preparingTime, t, tableId);
                    _foodOrders.Add(tmp);
                }

                var timeNow = DateTime.Now;
                AddResultText($"Заказ: {tmp}. Время заказа: {timeNow}. Будет готово " +
                              $"в {timeNow.Add(new TimeSpan(0,0,tmp.PreparingTime))}");
                Thread.Sleep(15000);
            }
        }

        //Consumer
        private void FinishFoodOrder(Info c, int id)
        {
            c.NameOfPerson = $"Повар - {id}";
            
            foreach (var v in _foodOrders.GetConsumingEnumerable())
            {
                c.UpdateTableInfo(v);
                ShowInfoForProgressBar(v.PreparingTime,c.ShowProgressBarPercents);
                AddResultText($"Выполнено (еда): {v} By {c.NameOfPerson} For Table №{v.TableNumber}");
            }
        }

        private void FinishDrinkOrder(Info c, int id)
        {
            c.NameOfPerson = $"Бармен - {id}";

            foreach (var v in _drinkOrders.GetConsumingEnumerable())
            {
                c.UpdateTableInfo(v);
                ShowInfoForProgressBar(v.PreparingTime, c.ShowProgressBarPercents);
                AddResultText($"Выполнено (напиток): {v} By {c.NameOfPerson} For Table №{v.TableNumber}");
            }
        }

        private void AddResultText(string text)
        {
            txtRes.InvokeIfRequired(() => txtRes.AppendText($"{text} " + Environment.NewLine));
            
        }

        private void ShowInfoForProgressBar(int time, Action<int> func)
        {
            double step =  time / 100.0;
            for (int i = 1; i <= 100; i++)
            {
                func(i);
                Thread.Sleep((int)(1000 * step));
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            lblOstatok.Text = $"Ожидается: Еда: {_foodOrders.Count}, Напитки: {_drinkOrders.Count}";

        }
    }
}
