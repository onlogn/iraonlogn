﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StolovkaTask
{
    public partial class Info : UserControl
    {
        public string NameOfPerson { get; set; }
        

        public Info()
        {
            InitializeComponent(); 
        }

        public void UpdateTableInfo(FoodOrder order)
        {
            this.InvokeIfRequired(() =>
            {
                lblCurTable.Text = $"Стол №{order.TableNumber}. {this.NameOfPerson}";
                lblFoodInfo.Text = order.Name;
            });
        }

        public void ShowProgressBarPercents(int current)
        {
            progressBar.InvokeIfRequired(() =>
            {
                progressBar.Value = current;
                lblProgress.Text = $"{current}%";
            });
        }
    }
}
