﻿namespace StolovkaTask
{
    partial class Info
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCurTable = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.lblProgress = new System.Windows.Forms.Label();
            this.lblFoodInfo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblCurTable
            // 
            this.lblCurTable.AutoSize = true;
            this.lblCurTable.Location = new System.Drawing.Point(3, 9);
            this.lblCurTable.Name = "lblCurTable";
            this.lblCurTable.Size = new System.Drawing.Size(49, 13);
            this.lblCurTable.TabIndex = 0;
            this.lblCurTable.Text = "Столик: ";
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(3, 25);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(246, 23);
            this.progressBar.TabIndex = 1;
            // 
            // lblProgress
            // 
            this.lblProgress.AutoSize = true;
            this.lblProgress.Location = new System.Drawing.Point(255, 30);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(24, 13);
            this.lblProgress.TabIndex = 2;
            this.lblProgress.Text = "0 %";
            // 
            // lblFoodInfo
            // 
            this.lblFoodInfo.AutoSize = true;
            this.lblFoodInfo.Location = new System.Drawing.Point(3, 55);
            this.lblFoodInfo.Name = "lblFoodInfo";
            this.lblFoodInfo.Size = new System.Drawing.Size(32, 13);
            this.lblFoodInfo.TabIndex = 3;
            this.lblFoodInfo.Text = "Еда: ";
            // 
            // Info
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblFoodInfo);
            this.Controls.Add(this.lblProgress);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.lblCurTable);
            this.Name = "Info";
            this.Size = new System.Drawing.Size(286, 77);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCurTable;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label lblProgress;
        private System.Windows.Forms.Label lblFoodInfo;
    }
}
