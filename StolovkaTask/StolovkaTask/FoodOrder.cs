﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StolovkaTask
{
    public class FoodOrder
    {
        public string Name { get; private set; }
        public FoodType Type { get; private set; } 
        public int PreparingTime { get; private set; }
        public int TableNumber { get; private set; }
        
        public FoodOrder(string name, int time, FoodType f, int tableNumber)
        {
            Name = name;
            PreparingTime = time;
            Type = f;
            TableNumber = tableNumber;
        }

        public FoodOrder(FoodOrder a) : this(a.Name, a.PreparingTime, a.Type, a.TableNumber)
        {

        }

        public override string ToString()
        {
            return $"Тип: {Type}. Название: {Name}. Время приготовления блюда: {PreparingTime}";
        }
    }
}
