﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace orders
{
    class Program
    {
        static void Main(string[] args)
        {
            var arr = new[]
            {
                new Order() { CustomerId = 1, OrderCost = 10 },
                new Order() { CustomerId = 1, OrderCost = 20 },
                new Order() { CustomerId = 2, OrderCost = 5 },
                new Order() { CustomerId = 3, OrderCost = 25 }
            };

            var res = arr.GroupBy(c => c.CustomerId)
                .Select(x => new
                {
                    CustomerId = x.Key,
                    OrderCost = x.Sum(a => a.OrderCost)
                })
                .OrderByDescending(z => z.OrderCost);

            foreach (var c in res)
            {
                Console.WriteLine($"{c.CustomerId} - {c.OrderCost}");
            }
        }
    }

    class Order
    {
        public int CustomerId { get; set; }
        public decimal OrderCost { get; set; }
    }

    //вывести в порядке убывания всех клиентов: клиент - сумма его закозов
}
