﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex2
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            for (int i = 0; i < n; i++)
            {
                //string str = Console.ReadLine();
                string str = @"Computers account for only 5% of the country's commercial electricity consumption.".ToLower();

                var sd = new SortedDictionary<char, int>();
                int maxValue = 0;
                for (int j = 0; j < str.Length; j++)
                {
                    if (!Char.IsLetter(str[j])) continue;
                    if (!sd.ContainsKey(str[j]))
                    {
                        // sd[str[j]] = sd.ElementAtOrDefault(str[j], 0) + 1;
                        sd[str[j]] = 0;
                    }

                    int t = sd[str[j]]++;
                    maxValue = maxValue < t ? t : maxValue;
                }

                foreach (var z in sd)
                {
                    if (z.Value == maxValue)
                    {
                        Console.Write(z.Key);
                    }
                }
                Console.WriteLine();
            }
        }
    }

    public static class StringExtension
    {
        public static string DictToStr(this SortedDictionary<char, int> sd)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var v in sd)
            {
                sb.Append($"{v.Key} => {v.Value}\n");
            }

            return sb.ToString();
        }
    }
}
