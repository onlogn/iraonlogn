﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Events4
{
    public class Bank
    {
        public event Action<decimal,Account> BalanceChanged;
        private Random _rnd = new Random();

        public void ChangeBalance(Account[] acc)
        {
            while (true)
            {
                decimal d = _rnd.Next(-100, 101);
                int idxacc = _rnd.Next(acc.Length);
                var current = acc[idxacc];
                if (current.Balance > current.Credit * -1)
                {
                    current.EditBalance(d);
                    OnBalanceChanged(d, current);
                    Thread.Sleep(1000);
                }
            }
        }


        protected virtual void OnBalanceChanged(decimal arg1, Account arg2)
        {
            BalanceChanged?.Invoke(arg1, arg2);
        }
    }
}
