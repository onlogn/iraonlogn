﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Events4
{
    class Program
    {
        
        private static Account[] arrAccounts = new[]
        {
            new Account(1,"First",200,300),
            new Account(2,"Second",500,100),
            new Account(3,"Third",50,20),
            new Account(4,"Next4",10,0),
            new Account(5,"Next5",200,400)
        };

        private static Bank b = new Bank();
        private static ConsoleWriter c = new ConsoleWriter();

        static void Main(string[] args)
        {
            b.BalanceChanged += (item,a) =>
            {
                if (a.Balance < a.Credit * -1)
                {
                    a.BalanceShown -= c.SHowAccount;
                    Console.WriteLine($"Otletaet: {a.Name}");
                }
                c.ShowInfo(item,a);
            };

            foreach (var v in arrAccounts)
            {
                v.BalanceShown += c.SHowAccount;
            }
            

            b.ChangeBalance(arrAccounts);
            
        }

    }
}
