﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events4
{
    public class ConsoleWriter
    {
        public void SHowAccount(Account acc)
        {
            Console.WriteLine($"Name: {acc.Name}, Balance: {acc.Balance}");
        }

        public void ShowInfo(decimal d, Account acc)
        {
            Console.WriteLine($"Name: {acc.Name}, Prev: {acc.Balance - d} + {d}");  
        }
    }
}
