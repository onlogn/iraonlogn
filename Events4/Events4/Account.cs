﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Events4
{
    public class Account
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public decimal Balance { get; private set; }
        public decimal Credit { get; private set; }

        public event Action<Account> BalanceShown;

        private Timer _t; 

        public Account(int id, string name, decimal balance, decimal credit)
        {
            Id = id;
            Name = name;
            Balance = balance;
            Credit = credit;

            _t = new Timer(CallBack, null, 0, 5000);
        }

        public void EditBalance(decimal n)
        {
            Balance += n;
        }

        protected virtual void OnBalanceShown(Account obj)
        {
            BalanceShown?.Invoke(obj);
        }

        private void CallBack(object o)
        {
            OnBalanceShown(this);
        }
    }
}
