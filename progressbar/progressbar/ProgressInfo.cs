﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progressbar
{
    public class ProgressInfo
    {
        public int PercentComplete { get; set; }
    }
}
