﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace progressbar
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private Task ProcessData(List<string> list, IProgress<ProgressInfo> pinf)
        {
            int idx = 1;
            int totalProcess = list.Count;
            ProgressInfo progInfo = new ProgressInfo();
            return Task.Run(() =>
            {
                for (int i = 0; i < totalProcess; i++)
                {
                    progInfo.PercentComplete = idx++ * 100 / totalProcess;
                    pinf.Report(progInfo);
                    Thread.Sleep(10);
                }
            });
        }

        private async void btnStart_Click(object sender, EventArgs e)
        {
            List<string> lst = Enumerable.Range(0, 1000).Select(i => i.ToString()).ToList();
            lblStatus.Text = "Настроение...";
            var progress = new Progress<ProgressInfo>();
            progress.ProgressChanged += (o, report) =>
            {
                lblStatus.Text = $"Настроение... {report.PercentComplete}%";
                progrBarWork.Value = report.PercentComplete;
                progrBarWork.Update();
            };
            await ProcessData(lst, progress); 
            lblStatus.Text = "Настроение поднято!";
        }
    }
}
