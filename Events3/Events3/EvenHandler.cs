﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events3
{
    public class EvenHandler
    {
        public void ShowEven(object o, NumberChangedEventArgs num)
        {
            Console.WriteLine($"Even: {num.N}");
        }
    }
}
