﻿using System;

namespace Events3
{
    public class NumberChangedEventArgs : EventArgs
    {
        public int N { get; set; }
    }
}