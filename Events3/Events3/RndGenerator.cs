﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Events3
{
    public class RndGenerator
    {
        private Random _rnd = new Random();

        public event EventHandler<NumberChangedEventArgs> EvenGenerated;
        public event EventHandler<NumberChangedEventArgs> OddGenerated; 

        public void GenerateNumber()
        {
            while (true)
            {
                int n = _rnd.Next(100);
                NumberChangedEventArgs c = new NumberChangedEventArgs() {N = n};
                if (n % 2 == 0)
                {
                    OnEvenGenerated(c);
                }
                else
                {
                    OnOddGenerated(c);
                }

                Console.WriteLine(n);
                Thread.Sleep(1000);
            }
        }

        protected virtual void OnEvenGenerated(NumberChangedEventArgs obj)
        {
            EvenGenerated?.Invoke(this,obj);
        }

        protected virtual void OnOddGenerated(NumberChangedEventArgs obj)
        {
            OddGenerated?.Invoke(this, obj);
        }
    }
}
