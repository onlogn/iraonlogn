﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Events3
{
    class Program
    {
        private static int counter;
        private static RndGenerator r = new RndGenerator();
        private static OddHandler oh = new OddHandler();
        private static EvenHandler eh = new EvenHandler();

        static void Main(string[] args)
        {
            Timer t = new Timer(TimerTick,null,0,10000);
            r.GenerateNumber();
        }

        private static void TimerTick(object obj)
        {
            counter++;
            if (counter % 4 == 1)
            {
                r.EvenGenerated += eh.ShowEven;
            } else if (counter % 4 == 2)
            {
                r.EvenGenerated -= eh.ShowEven;
                r.OddGenerated += oh.ShowOdd;
            } else if (counter % 4 == 3)
            {
                r.EvenGenerated += eh.ShowEven;
            } else if (counter % 4 == 0)
            {
                r.EvenGenerated -= eh.ShowEven;
                r.OddGenerated -= oh.ShowOdd;
            }
        }
    }
}
