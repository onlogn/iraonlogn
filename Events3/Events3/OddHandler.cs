﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events3
{
    public class OddHandler
    {
        public void ShowOdd(object o, NumberChangedEventArgs num)
        {
            Console.WriteLine($"Odd: {num.N}");
        }
    }
}
