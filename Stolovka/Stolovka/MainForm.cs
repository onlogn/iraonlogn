﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Stolovka
{
    public partial class MainForm : Form
    {
        private BlockingCollection<Food> _foodOrders = new BlockingCollection<Food>();
        private BlockingCollection<Food> _drinkOrders = new BlockingCollection<Food>();
       
        private readonly string[] _food = new[] {"Meal", "Juice"};
        private Random _rnd = new Random();

        private DateTime timeNow;
        private int _currentTime = 0;

        public MainForm()
        {
            InitializeComponent();

            timeNow = new DateTime();
            
            int numOfTables = 5;
            for (int i = 0; i < numOfTables; i++)
            {
                var t = new Thread(DoOrder);
                t.Name = $"Stolik {i}: ";
                t.IsBackground = true;
                t.Start();
            }

            Thread.Sleep(100);

            int numOfCoockers = 3;
            for (int i = 0; i < numOfCoockers; i++)
            {
                var x = new Thread(FinishFoodOrder);
                x.Name = $"Povar {i}: ";
                x.IsBackground = true;
                x.Start();
            }

            int numOfBarmens = 2;
            for (int i = 0; i < numOfBarmens; i++)
            {
                var b = new Thread(FinishDrinkOrder);
                b.Name = $"Barmen {i}: ";
                b.IsBackground = true;
                b.Start();
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            
        }

        //Producer
        private void DoOrder()
        {
            while (true)
            {
                Food tmp = new Food(_food[_rnd.Next(2)],_rnd.Next(10));
                if (tmp.Name == "Juice")
                {
                    _drinkOrders.Add(tmp);
                }
                else
                {
                    _foodOrders.Add(tmp);
                }
                
                lblRes.Text += Thread.CurrentThread.Name + tmp + "\n";
                Thread.Sleep(3000);
            }
        }

        //Consumer
        private void FinishFoodOrder()
        {
            foreach (var v in _foodOrders.GetConsumingEnumerable())
            {
                lblRes.Text += $"{Thread.CurrentThread.Name}{v}. Order will complete at {_currentTime}\n";
            }
        }

        private void FinishDrinkOrder()
        {
            foreach (var v in _drinkOrders.GetConsumingEnumerable())
            {
                lblRes.Text += $"{Thread.CurrentThread.Name}{v}. Order will complete at {_currentTime}\n";
            }
        }
    }
}
