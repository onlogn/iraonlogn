﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stolovka
{
    public class Food
    {
        public string Name { get; private set; }
        public int PreparingTime { get; private set; }

        public Food(string name, int time)
        {
            Name = name;
            PreparingTime = time;
        }

        public Food(Food a):this(a.Name,a.PreparingTime)
        {
            
        }

        public override string ToString()
        {
            return $"Название: {Name}. Время приготовления блюда: {PreparingTime}";
        }
    }
}
