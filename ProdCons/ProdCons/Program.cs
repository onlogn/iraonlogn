﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProdCons
{
    class Program
    {
        static public void Main()
        {
            new Program().Run();
        }

        BlockingCollection<string> q = new BlockingCollection<string>();

        void Run()
        {
            var threads = new[] { new Thread(Consumer), new Thread(Consumer) };
            foreach (var t in threads)
                t.Start();

            string s;
            while ((s = Console.ReadLine()).Length != 0)
                q.Add(s);

            q.CompleteAdding(); // останавливаем

            foreach (var t in threads)
                t.Join();
        }

        void Consumer()
        {
            foreach (var s in q.GetConsumingEnumerable())
            {
                Console.WriteLine("Processing: {0}", s);
                Thread.Sleep(2000);
                Console.WriteLine("Processed: {0}", s);
            }
        }
    }
}
