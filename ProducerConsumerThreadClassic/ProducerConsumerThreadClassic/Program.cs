﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProducerConsumerThreadClassic
{
    class Program
    {
        private static Random _rnd = new Random();

        //автоматический подниматель/опускатель "флажка"/"шлагбаума", чтобы пропускать потоки по очереди
        private static AutoResetEvent _auto = new AutoResetEvent(false);

        private static Queue<int> _queue = new Queue<int>();

        static void Main(string[] args)
        {
            var t1 = new Thread(Producer);
            t1.Name = "Producer1";
            t1.Start();

            var c1 = new Thread(Consumer);
            c1.Name = "Consumer1";
            c1.Start();

            var c2 = new Thread(Consumer);
            c2.Name = "Consumer2";
            c2.Start();
        }

        private static void Producer()
        {
            while (true)
            {
                int tmp = _rnd.Next(10);
                Console.WriteLine($"{Thread.CurrentThread.Name}: {tmp}");
                _queue.Enqueue(tmp);

                // поднять флажок/шлагбаум чтобы пропустить 1 в очередь
                _auto.Set();

                Thread.Sleep(1000);
            }
        }

        private static void Consumer()
        {
            while (true)
            {
                //ждать пока кто-то пройдет и закрыть шлагбаум/опустить флажок, чтобы остальные ждали
                _auto.WaitOne();

                int tmp = _queue.Dequeue();
                Console.WriteLine($"Поток {Thread.CurrentThread.Name}. {tmp} * {tmp} = {tmp * tmp}");
            }
        }
    }
}