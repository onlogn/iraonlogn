﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace m1
{
    class Program
    {

        private static Random rnd = new Random();

        static void Main(string[] args)
        {
            StringBuilder v = new StringBuilder();
            StringBuilder ans = new StringBuilder();

           for (int i = 0; i < 9; i++)
            {
                int mult = rnd.Next(-5, 6);
                v.AppendLine($"Фамилия Имя __________________________________________________");
                v.AppendLine($"Вариант №{i}");
                ans.AppendLine($"Вариант №{i}");

                v.AppendLine($"Задание 1. Найдите \n1.1) A+B, \n1.2) A-B, \n1.3) {mult}*A \n1.4) A*B \n1.5) B*A");
                ans.AppendLine("№1");

                Matrix a = GenerateMatrix(2);
                Matrix b = GenerateMatrix(2);

                v.AppendLine("A = ");
                v.AppendLine(a.ToString());

                v.AppendLine("B = ");
                v.AppendLine(b.ToString());

                ans.AppendLine((a + b).ToString());
                ans.AppendLine((a - b).ToString());
                ans.AppendLine((mult*a).ToString());
                ans.AppendLine((a*b).ToString());
                ans.AppendLine((b*a).ToString());

                v.AppendLine($"Задание 2. Вычислите определители:");
                ans.AppendLine("№2");


                Matrix det1 = GenerateMatrix(2);
                Matrix det2 = GenerateMatrix(2);
                Matrix det3 = GenerateMatrix(3);
                Matrix det4 = GenerateMatrix(3);
                Matrix det5 = GenerateMatrix(3);

                v.AppendLine("1");
                v.AppendLine(det1.ToString());
                v.AppendLine("2");
                v.AppendLine(det2.ToString());
                v.AppendLine("3");
                v.AppendLine(det3.ToString());
                v.AppendLine("4");
                v.AppendLine(det4.ToString());
                v.AppendLine("5");
                v.AppendLine(det5.ToString());

                ans.AppendLine(det1.Determinant().ToString());
                ans.AppendLine(det2.Determinant().ToString());
                ans.AppendLine(det3.Determinant().ToString());
                ans.AppendLine(det4.Determinant().ToString());
                ans.AppendLine(det5.Determinant().ToString());

            }

        File.AppendAllText(@"C:\iraonlogn\m1\tasks.txt",v.ToString());
        File.AppendAllText(@"C:\iraonlogn\m1\ans.txt",ans.ToString());

        }

        static Matrix GenerateMatrix(int dimension)
        {
            Matrix a = new Matrix(dimension, dimension);

            for (int i = 0; i < dimension; i++)
            {
                for (int j = 0; j < dimension; j++)
                {
                    a[i, j] = rnd.Next(-10, 11);
                }
            }

            return a;
        }


    }
}
