﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProducerConsumerBlockCollection
{
    class Program
    {
        private static Random _rnd = new Random();
        private static BlockingCollection<int> list = new BlockingCollection<int>();

        static void Main(string[] args)
        {
            var p1 = new Thread(Producer);
            p1.Name = "Producer1";
            p1.Start();

            var p2 = new Thread(Producer);
            p2.Name = "Producer2";
            p2.Start();

            var c1 = new Thread(Consumer);
            c1.Name = "Consumer1";
            c1.Start();

            var c2 = new Thread(Consumer);
            c2.Name = "Consumer2";
            c2.Start();

        }

        private static void Producer()
        {
            while (true)
            {
                int tmp = _rnd.Next(20);
                list.Add(tmp);
                Console.WriteLine($"{Thread.CurrentThread.Name}: {tmp}");
                Thread.Sleep(3000);
            }
        }

        private static void Consumer()
        {
            foreach (var v in list.GetConsumingEnumerable())
            {
                Console.WriteLine($"{Thread.CurrentThread.Name}: {v} * {v} = {v*v}");
            }
        }
    }
}
