﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InvokeRequiredTesting
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            var t = new Thread(() =>
            {
                Func1();
                Func2("MyText");
            });
            t.IsBackground = true;
            t.Start();
        }

        private void Func1()
        {
            btn.InvokeIfRequired(() => { btn.Text = "text666"; });
        }

        private void Func2(string text)
        {
            this.InvokeIfRequired(() => btn.Text = text);
        }

        
    }
}
