﻿namespace Skb
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.lblField = new System.Windows.Forms.Label();
            this.btnNewGame = new System.Windows.Forms.Button();
            this.btnRestartLevel = new System.Windows.Forms.Button();
            this.lblDescription = new System.Windows.Forms.Label();
            this.picField = new System.Windows.Forms.PictureBox();
            this.lblLvlInfo = new System.Windows.Forms.Label();
            this.btnStatistics = new System.Windows.Forms.Button();
            this.btnHints = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picField)).BeginInit();
            this.SuspendLayout();
            // 
            // lblField
            // 
            this.lblField.AutoSize = true;
            this.lblField.Font = new System.Drawing.Font("Courier New", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblField.Location = new System.Drawing.Point(65, 33);
            this.lblField.Name = "lblField";
            this.lblField.Size = new System.Drawing.Size(0, 30);
            this.lblField.TabIndex = 0;
            // 
            // btnNewGame
            // 
            this.btnNewGame.Location = new System.Drawing.Point(201, 412);
            this.btnNewGame.Name = "btnNewGame";
            this.btnNewGame.Size = new System.Drawing.Size(152, 31);
            this.btnNewGame.TabIndex = 1;
            this.btnNewGame.TabStop = false;
            this.btnNewGame.Text = "Новая игра";
            this.btnNewGame.UseVisualStyleBackColor = true;
            this.btnNewGame.Click += new System.EventHandler(this.btnNewGame_Click);
            // 
            // btnRestartLevel
            // 
            this.btnRestartLevel.Location = new System.Drawing.Point(375, 412);
            this.btnRestartLevel.Name = "btnRestartLevel";
            this.btnRestartLevel.Size = new System.Drawing.Size(175, 30);
            this.btnRestartLevel.TabIndex = 2;
            this.btnRestartLevel.TabStop = false;
            this.btnRestartLevel.Text = "Начать уровень сначала";
            this.btnRestartLevel.UseVisualStyleBackColor = true;
            this.btnRestartLevel.Click += new System.EventHandler(this.btnRestartLevel_Click);
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblDescription.Location = new System.Drawing.Point(232, 446);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(276, 13);
            this.lblDescription.TabIndex = 3;
            this.lblDescription.Text = "W A S D - двигаться, V - пропустить уровень";
            // 
            // picField
            // 
            this.picField.Location = new System.Drawing.Point(25, 33);
            this.picField.Name = "picField";
            this.picField.Size = new System.Drawing.Size(561, 370);
            this.picField.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picField.TabIndex = 4;
            this.picField.TabStop = false;
            this.picField.Visible = false;
            this.picField.SizeChanged += new System.EventHandler(this.picField_SizeChanged);
            // 
            // lblLvlInfo
            // 
            this.lblLvlInfo.AutoSize = true;
            this.lblLvlInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLvlInfo.Location = new System.Drawing.Point(332, 17);
            this.lblLvlInfo.Name = "lblLvlInfo";
            this.lblLvlInfo.Size = new System.Drawing.Size(0, 13);
            this.lblLvlInfo.TabIndex = 5;
            // 
            // btnStatistics
            // 
            this.btnStatistics.Location = new System.Drawing.Point(598, 413);
            this.btnStatistics.Name = "btnStatistics";
            this.btnStatistics.Size = new System.Drawing.Size(128, 30);
            this.btnStatistics.TabIndex = 6;
            this.btnStatistics.TabStop = false;
            this.btnStatistics.Text = "Рейтинги";
            this.btnStatistics.UseVisualStyleBackColor = true;
            this.btnStatistics.Click += new System.EventHandler(this.btnStatistics_Click);
            // 
            // btnHints
            // 
            this.btnHints.Location = new System.Drawing.Point(25, 413);
            this.btnHints.Name = "btnHints";
            this.btnHints.Size = new System.Drawing.Size(142, 30);
            this.btnHints.TabIndex = 7;
            this.btnHints.TabStop = false;
            this.btnHints.UseVisualStyleBackColor = true;
            this.btnHints.Click += new System.EventHandler(this.btnHints_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(749, 484);
            this.Controls.Add(this.btnHints);
            this.Controls.Add(this.btnStatistics);
            this.Controls.Add(this.lblLvlInfo);
            this.Controls.Add(this.picField);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.btnRestartLevel);
            this.Controls.Add(this.btnNewGame);
            this.Controls.Add(this.lblField);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Сокобан";
            this.Load += new System.EventHandler(this.SokobanGame_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SokobanGame_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.picField)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblField;
        private System.Windows.Forms.Button btnNewGame;
        private System.Windows.Forms.Button btnRestartLevel;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.PictureBox picField;
        private System.Windows.Forms.Label lblLvlInfo;
        private System.Windows.Forms.Button btnStatistics;
        private System.Windows.Forms.Button btnHints;
    }
}

