﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Skb
{
    class Sokoban
    {
        public int[,] Field { get; private set; }
        public List<Point> BoxPos { get; private set; }
        public List<Point> DestinationPos { get; private set; }
        public Point PlayerPos { get; private set; }
        public bool GameStatus { get; private set; }
        public int NumberOfSteps { get; private set; }
        public bool Hints;
        private bool _failFlag;
        private List<Point> _hintList;
        private List<List<Point>> _hintBoxList;

        private Random _rnd = new Random();
        List<Bitmap> failsPics = new List<Bitmap>()
        {
            Properties.Resources.playerfail1,
            Properties.Resources.playerfail2,
            Properties.Resources.playerfail3,
            Properties.Resources.playerfail4
        };

        private int _idx;
        
        private enum FieldItem
        {
            Space = 0,
            Wall = 1
        }

        public Sokoban(string fileLvl, string fileHint)
        {
            BoxPos = new List<Point>();
            DestinationPos = new List<Point>();
            _hintList = new List<Point>();
            _hintBoxList = new List<List<Point>>();
            ReadLvl(fileLvl);
            ReadHints(fileHint);
            GameStatus = true;
            NumberOfSteps = 0;
            _idx = _rnd.Next(0, failsPics.Count);
            Hints = false;
            _failFlag = false;
        }

        public void Move(Keys key)
        {
            if (!GameStatus) return;

            Point nextPlayerPos = PlayerPos + GetDirectionPoint(key);
            int ixPlayerOnBox = BoxPos.IndexOf(nextPlayerPos);
            
            if (IsOutOfRange(nextPlayerPos) || Field[nextPlayerPos.Row,nextPlayerPos.Column] == (int)FieldItem.Wall) return;

            if (ixPlayerOnBox != -1)
            {
                Point nextBoxPos = BoxPos[ixPlayerOnBox] + GetDirectionPoint(key);
                //ящик впечатываем в стенку ИЛИ ящик за пределами массива ИЛИ ящик в другой ящик
                if (Field[nextBoxPos.Row, nextBoxPos.Column] == (int)FieldItem.Wall || IsOutOfRange(nextBoxPos) || BoxPos.Contains(nextBoxPos)) return;

                BoxPos[ixPlayerOnBox] = nextBoxPos;
            }
            PlayerPos = nextPlayerPos;

            if(!GetDirectionPoint(key).Equals(new Point(0,0))) NumberOfSteps++;

            if (IsGameOver())
            {
                GameStatus = false;
            }

            if (!_failFlag && (BoxPos.Intersect(_hintList).Any()) || IsRestrictedPos())
            {
                _failFlag = true;
            }
        }

        private bool IsGameOver()
        {
            return !BoxPos.Except(DestinationPos).Any();
        }

        private bool IsOutOfRange(Point a)
        {
            return a.Row >= Field.GetLength(0) || a.Row < 0 || a.Column >= Field.GetLength(1) || a.Column < 0;
        }

        private Point GetDirectionPoint(Keys key)
        {
            Point res = new Point(0, 0);

            switch (key)
            {
                case Keys.A:
                case Keys.Left:
                    res = new Point(0, -1);
                    break;
                case Keys.Right:
                case Keys.D:
                    res = new Point(0, 1);
                    break;
                case Keys.Up:
                case Keys.W:
                    res = new Point(-1, 0);
                    break;
                case Keys.Down:
                case Keys.S:
                    res = new Point(1, 0);
                    break;
                default:
                    res = new Point(0, 0);
                    break;
            }

            return res;
        }


        public Bitmap ShowPicField()
        {
            const int sizeStep = 32; 

            int imageRows = Field.GetLength(0);
            int imageColumns = Field.GetLength(1);
            
            Bitmap bm = new Bitmap(imageColumns * sizeStep, imageRows * sizeStep);
            Graphics g = Graphics.FromImage(bm);

            for (int i = 0; i < imageRows; i++)
            {
                for (int j = 0; j < imageColumns; j++)
                {
                    if (i == PlayerPos.Row && j == PlayerPos.Column)
                    {
                        if (Hints && _failFlag)
                        {
                            g.DrawImage(failsPics[_idx],
                                new System.Drawing.Point(j * sizeStep, i * sizeStep));
                        }
                        else
                        {
                            g.DrawImage(Properties.Resources.player,
                                new System.Drawing.Point(j * sizeStep, i * sizeStep));
                        }
                    }
                    else if (BoxPos.Contains(new Point(i, j)))
                    {
                        g.DrawImage(Properties.Resources.box, new System.Drawing.Point(j * sizeStep, i * sizeStep));
                    }
                    else if (DestinationPos.Contains(new Point(i, j)))
                    {
                        g.DrawImage(Properties.Resources.dest, new System.Drawing.Point(j * sizeStep, i * sizeStep));
                    }
                    else if (Field[i, j] == (int)FieldItem.Wall)
                    {
                        g.DrawImage(Properties.Resources.wall, new System.Drawing.Point(j * sizeStep, i * sizeStep));
                    }
                    else if (Field[i, j] == (int)FieldItem.Space)
                    {
                        g.DrawImage(Properties.Resources.texture, new System.Drawing.Point(j * sizeStep, i * sizeStep)); ;
                    }
                }
            }
            

            return bm;
        }

        public string ShowField()
        {
            StringBuilder sb = new StringBuilder();

            for(int i = 0; i < Field.GetLength(0); i++)
            {
                for (int j = 0; j < Field.GetLength(1); j++)
                {
                    if (i == PlayerPos.Row && j == PlayerPos.Column)
                    {
                        sb.Append("+");
                    } else if (BoxPos.Contains(new Point(i,j)))
                    {
                        sb.Append("O");
                    } else if (DestinationPos.Contains(new Point(i, j)))
                    {
                        sb.Append("X");
                    }
                    else if (Field[i, j] == (int) FieldItem.Wall)
                    {
                        sb.Append("#");
                    } else if (Field[i, j] == (int) FieldItem.Space)
                    {
                        sb.Append(" ");
                    }
                }
                sb.AppendLine();
            }

            return sb.ToString();
        }

        private void ReadLvl(string file)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            
            using (Stream stream = assembly.GetManifestResourceStream(file))
            {
                using (StreamReader sr = new StreamReader(stream, System.Text.Encoding.Default))
                {
                    string[] matrix = sr.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                    int numOfRows = int.Parse(matrix[0]);
                    int numOfColumns = int.Parse(matrix[1]);

                    Field = new int[numOfRows, numOfColumns];
                    for (int i = 0; i < numOfRows; i++)
                    {
                        int[] tmp = sr.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries)
                            .Select(int.Parse).ToArray();
                        for (int j = 0; j < numOfColumns; j++)
                        {
                            Field[i, j] = tmp[j];
                        }
                    }

                    int numOfBoxes = int.Parse(sr.ReadLine());
                    for (int i = 0; i < numOfBoxes; i++)
                    {
                        int[] tmp = sr.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries)
                            .Select(int.Parse)
                            .ToArray();
                        BoxPos.Add(new Point(tmp[0], tmp[1]));
                    }

                    for (int i = 0; i < numOfBoxes; i++)
                    {
                        int[] tmp = sr.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries)
                            .Select(int.Parse)
                            .ToArray();
                        DestinationPos.Add(new Point(tmp[0], tmp[1]));
                    }

                    int[] player = sr.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries)
                        .Select(int.Parse)
                        .ToArray();
                    PlayerPos = new Point(player[0], player[1]);
                }
            }
        }

        private void ReadHints(string file)
        {
            try
            {
                Assembly assembly = Assembly.GetExecutingAssembly();
                using (Stream stream = assembly.GetManifestResourceStream(file))
                {
                    using (StreamReader sr = new StreamReader(stream, System.Text.Encoding.Default))
                    {
                        int numOfHints = int.Parse(sr.ReadLine());
                        for (int i = 0; i < numOfHints; i++)
                        {
                            var tmpLine = sr.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries)
                                .Select(int.Parse).ToList();
                            _hintList.Add(new Point(tmpLine[0], tmpLine[1]));
                        }

                        int numOfBoxHints = int.Parse(sr.ReadLine());
                        for (int i = 0; i < numOfBoxHints; i++)
                        {
                            List<Point> tmpList = new List<Point>();
                            for (int j = 0; j < 4; j++)
                            {
                                
                                var tmpLine = sr.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries)
                                    .Select(int.Parse).ToList();
                                tmpList.Add(new Point(tmpLine[0], tmpLine[1]));
                            }
                            _hintBoxList.Add(tmpList);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                
            }
        }

        private bool IsRestrictedPos()
        {
            bool flag = false;

            foreach (var i in _hintBoxList)
            {
                if (!flag && BoxPos.Intersect(i).Count() == 4)
                {
                    flag = true;
                }
            }

            return flag;
        }
    }
}
