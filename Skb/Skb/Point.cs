﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skb
{
    class Point
    {
        public int Row { get; private set; }
        public int Column { get; private set; }

        public Point(int row, int col)
        {
            Row = row;
            Column = col;
        }

        public Point(Point a): this(a.Row, a.Column)
        {
            
        }

        public static Point operator +(Point a, Point b)
        {
            return new Point(a.Row + b.Row, a.Column + b.Column);
        }

        private bool Equals(Point a)
        {
            if (ReferenceEquals(null, a)) return false;
            if (ReferenceEquals(this, a)) return true;
            return Equals(a.Row, Row) && Equals(a.Column, Column);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Point)) return false;
            return Equals((Point)obj);
        }

        public override int GetHashCode()
        {
            int num = 13;

            var res = Row.GetHashCode();
            res = res * num + Column.GetHashCode();
            return res;
        }
    }
}
