namespace Skb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStatsMigration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.StatisticsInfoes", "Name", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.StatisticsInfoes", "Name", c => c.Int(nullable: false));
        }
    }
}
