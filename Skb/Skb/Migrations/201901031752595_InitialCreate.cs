namespace Skb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StatisticsInfoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NumOfSteps = c.Int(nullable: false),
                        Name = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.StatisticsInfoes");
        }
    }
}
