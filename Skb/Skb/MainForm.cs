﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Skb.Properties;

namespace Skb
{
    public partial class MainForm : Form
    {
        private Sokoban _sk;
        private List<string> _lvls = new List<string>();
        private List<string> _hints = new List<string>();
        private const int _numOfLvls = 6;
        private int _numSteps;
        private bool _cheater;
        private int _curLvl;

        public MainForm()
        {
            InitializeComponent();
        }

        private void SokobanGame_Load(object sender, EventArgs e)
        {
            var assembly = Assembly.GetExecutingAssembly();
            
            for (int i = 1; i <= _numOfLvls; i++)
            {
                string resourceName = assembly.GetManifestResourceNames()
                    .Single(str => str.EndsWith($"lvl{i}.txt"));
                _lvls.Add(resourceName);
            }

            for (int i = 1; i <= _numOfLvls; i++)
            {
                string resourceName = assembly.GetManifestResourceNames()
                        .Single(str => str.EndsWith($"hint{i}.txt"));
                _hints.Add(resourceName);
            }

            lblDescription.Text += "\nДотащи голубые ящики до кружочков.";

            lblField.Location = new System.Drawing.Point(10,10);
            lblField.Text = "Дотащи голубые ящики до кружочков.\nW A S D - управление\nV - пропустить уровень";

            btnHints.Visible = false;

            btnHints.GotFocus += (sr, args) => picField.Focus();
            btnNewGame.GotFocus += (sr, args) => picField.Focus();
            btnRestartLevel.GotFocus += (sr, args) => picField.Focus();
            btnStatistics.GotFocus += (sr, args) => picField.Focus();

        }

        private void btnNewGame_Click(object sender, EventArgs e)
        {
            picField.Visible = true;
            lblField.Visible = false;
            btnHints.Visible = true;
            _curLvl = 0;
            StartLevel();
            ShowHintsBtnText();
            _numSteps = 0;
            _cheater = false;
            ShowField(_sk);
            ShowLvlInfo();
        }

        private void SokobanGame_KeyDown(object sender, KeyEventArgs e)
        {
        //    if (_sk == null) return;

        //    _sk.Move(e.KeyCode);
        //    ShowField(_sk);

        //    if (e.KeyCode == Keys.V) _cheater = true;

        //    if (!_sk.GameStatus || e.KeyCode == Keys.V)
        //    {
        //        IsGameOver();
        //    }

        //    ShowLvlInfo();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (_sk == null) return base.ProcessCmdKey(ref msg, keyData); 

            _sk.Move(keyData);
            ShowField(_sk);

            if (keyData == Keys.V) _cheater = true;

            if (!_sk.GameStatus || keyData == Keys.V)
            {
                IsGameOver();
            }

            ShowLvlInfo();

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void btnRestartLevel_Click(object sender, EventArgs e)
        {
            if (_sk == null) return;
            bool prevStatusHints = _sk.Hints;

            StartLevel();

            _sk.Hints = prevStatusHints;
            ShowHintsBtnText();

            ShowField(_sk);
        }

        private void ShowField(Sokoban sk)
        {
            picField.Image = _sk.ShowPicField();
            picField.Refresh();
        }

        private void ShowLvlInfo()
        {
            lblLvlInfo.Text = $"Уровень {_curLvl + 1} / {_numOfLvls}. Шагов: {_sk.NumberOfSteps}";
        }

        private bool IsGameComplete()
        {
            return _curLvl >= _numOfLvls - 1;
        }

        private void picField_SizeChanged(object sender, EventArgs e)
        {
            int sizestep = 32;
            int x = _sk.Field.GetLength(1) * sizestep;
            int res = (Width - x) / 2;

            picField.Location = new System.Drawing.Point(res, 40);
        }

        private void IsGameOver()
        {
            if (IsGameComplete())
            {
                string cheat = _cheater ? "CHEATER!" : "";
                _numSteps += _sk.NumberOfSteps;

                if (_cheater)
                {
                    MessageBox.Show($"Поздравляю! Вы прошли игру за {_numSteps} ход(-ов)! {cheat}. \nВ таблицу рекордов можно попасть не пропуская уровни!");
                }
                else
                {
                    NameInputForm nf = new NameInputForm(_numSteps);
                    nf.Show();
                }
                
                _curLvl = 0;
                StartLevel();
                ShowField(_sk);
                ShowLvlInfo();
                _numSteps = 0;
                ShowHintsBtnText();
                return;
            }

            _curLvl++;
            _numSteps += _sk.NumberOfSteps;
            bool tmpStatusHints = _sk.Hints;
            StartLevel();
            _sk.Hints = tmpStatusHints;
            ShowHintsBtnText();
            ShowField(_sk);
        }

        private void btnStatistics_Click(object sender, EventArgs e)
        {
            StatisticsForm sf = new StatisticsForm();
            sf.Show();
        }

        private void btnHints_Click(object sender, EventArgs e)
        {
            _sk.Hints = !_sk.Hints;
            ShowHintsBtnText();
        }

        private void ShowHintsBtnText()
        {
            btnHints.Text = _sk.Hints ? "Выключить смайлики" : "Включить смайлики";
        }

        private void StartLevel()
        {
            _sk = new Sokoban(_lvls[_curLvl], _hints[_curLvl]);
        }


    }
}
