﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Skb
{
    public partial class StatisticsForm : Form
    {
        public StatisticsForm()
        {
            InitializeComponent();
            dgvStats.AutoGenerateColumns = false;

            Task t = new Task(ShowDgvStats);
            t.Start();

            //Task t = Task.Run(() => ShowDgvStats());
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void StatisticsForm_Load(object sender, EventArgs e)
        {
            
        }


        private void ShowDgvStats()
        {
            List<StatisticsInfo> list;
            using (StatisticsInfoContext db = new StatisticsInfoContext())
            {
                list = db.StatInfo.OrderBy(i => i.NumOfSteps).ToList();
                dgvStats.InvokeIfRequired(() =>
                {
                    dgvStats.DataSource = list;
                });
            }
            
        }
    }
}
