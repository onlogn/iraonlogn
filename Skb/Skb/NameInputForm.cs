﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Skb
{
    public partial class NameInputForm : Form
    {
        private int _steps;

        public NameInputForm(int n)
        {
            InitializeComponent();
            _steps = n;
        }

        private async void btnOk_Click(object sender, EventArgs e)
        {
            string res = String.IsNullOrEmpty(txtName.Text.Trim())  ? "Anonimus" : txtName.Text.Trim();
            
            StatisticsInfo inf = new StatisticsInfo()
            {
                Name = res,
                NumOfSteps = _steps,
                Date = DateTime.Now
            };

            await Task.Run(() => AddInfoToDb(inf));

            Close();
        }

        private void AddInfoToDb(StatisticsInfo inf)
        {
            using (StatisticsInfoContext db = new StatisticsInfoContext())
            {
                db.StatInfo.Add(inf);
                db.SaveChanges();
            }
        }
    }
}
