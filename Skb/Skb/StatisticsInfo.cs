﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skb
{
    class StatisticsInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int NumOfSteps { get; set; }
        public DateTime Date { get; set; }
    }
}
