﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Skb
{
    class Crypter
    {
        /// <summary>
        /// Функция шифрования
        /// </summary>
        public static string EncryptString(string clearText, string Password)
        {
            string result = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(clearText))
                {
                    byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
                    PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                        new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    byte[] encryptedData = Encrypt(clearBytes, pdb.GetBytes(32), pdb.GetBytes(16));
                    result = Convert.ToBase64String(encryptedData);
                }
            }
            catch
            {

            }

            return result;
        }

        /// <summary>
        /// Функция расшифровки
        /// </summary>
        public static string DecryptString(string cipherText, string Password)
        {
            string result = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(cipherText))
                {
                    byte[] cipherBytes = Convert.FromBase64String(cipherText);
                    PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                        new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    byte[] decryptedData = Decrypt(cipherBytes, pdb.GetBytes(32), pdb.GetBytes(16));
                    result = Encoding.Unicode.GetString(decryptedData, 0, decryptedData.Length);
                }
            }
            catch
            {
            }

            return result;
        }


        private static byte[] Encrypt(byte[] clearData, byte[] Key, byte[] IV)
        {
            MemoryStream ms = new MemoryStream();

            //TripleDES alg = TripleDES.Create();
            Rijndael alg = Rijndael.Create();

            alg.Key = Key;
            alg.IV = IV;

            CryptoStream cs = new CryptoStream(ms, alg.CreateEncryptor(), CryptoStreamMode.Write);

            cs.Write(clearData, 0, clearData.Length);
            cs.Close();

            byte[] encryptedData = ms.ToArray();
            return encryptedData;
        }

        private static byte[] Decrypt(byte[] cipherData, byte[] Key, byte[] IV)
        {
            MemoryStream ms = new MemoryStream();

            //TripleDES alg = TripleDES.Create();
            Rijndael alg = Rijndael.Create();

            alg.Key = Key;
            alg.IV = IV;

            CryptoStream cs = new CryptoStream(ms, alg.CreateDecryptor(), CryptoStreamMode.Write);

            cs.Write(cipherData, 0, cipherData.Length);
            cs.Close();

            byte[] decryptedData = ms.ToArray();
            return decryptedData;
        }
    }
}
