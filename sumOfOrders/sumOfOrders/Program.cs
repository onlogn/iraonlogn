﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sumOfOrders
{
    class Program
    {
        static void Main(string[] args)
        {
            var arr = new[]
            {
                new CustomerInfo()
                {
                    Orders = new[]
                    {
                        new Order() {CustomerId = 1, OrderCost = 10},
                        new Order() {CustomerId = 1, OrderCost = 20}
                    }

                },
                new CustomerInfo()
                {
                    Orders = new[]
                    {
                        new Order() {CustomerId = 2, OrderCost = 5}
                    }
                },
                new CustomerInfo()
                {
                    Orders = new[]
                    {
                        new Order() {CustomerId = 3, OrderCost = 25}
                    }
                }
            };

            //найти 1) -сумму всех заказов
            var SumOfAllOrders = arr.SelectMany(o => o.Orders.Select(i => i.OrderCost)).Sum();

            Console.WriteLine($"Sum of all orders: {SumOfAllOrders}");


            // 2 - у какого клиента был самый дорогой заказ

            //var ress = arr.SelectMany(o => o.Orders.Select(i => i.OrderCost)).Max();
            var clientWithMaxOrder1 = arr.SelectMany(o => o.Orders, (i, c) => new {Cust = i, Cost = c}).OrderByDescending(i => i.Cost.OrderCost).Take(1);

            foreach (var i in clientWithMaxOrder1)
            {
                Console.WriteLine($"Max Order of customer 1: {i.Cost.CustomerId} is {i.Cost.OrderCost}");
            }

            var clientWithMaxOrder2 = arr.SelectMany(o => o.Orders.Select(i => new {Cust = i.CustomerId, Cost = i.OrderCost}))
                .OrderByDescending(i => i.Cost).Take(1);
                                
            foreach (var i in clientWithMaxOrder2)
            {
                Console.WriteLine($"Max Order of customer 2: {i.Cust} is {i.Cost}");
            }

            var clientWithMaxOrder3 = arr.SelectMany(o => o.Orders).OrderByDescending(i => i.OrderCost).Take(1);
            foreach (var i in clientWithMaxOrder3)
            {
                Console.WriteLine($"Max Order of customer 3: {i.CustomerId} is {i.OrderCost}");
            }

            //3 - клиент с самой большой суммой заказов
            var clientWithMaxSumOfOrders = arr.SelectMany(i => i.Orders).GroupBy(i => i.CustomerId)
                .Select(i => new {Customer = i.Key, AllCost = i.Sum(j => j.OrderCost)}).OrderByDescending(i => i.AllCost)
                .Take(1);

            foreach (var i in clientWithMaxSumOfOrders)
            {
                Console.WriteLine($"Max Sum customer: {i.Customer} is {i.AllCost}");
            }
        }

    }

    class Order
    {
        public int CustomerId { get; set; }
        public decimal OrderCost { get; set; }
    }

    class CustomerInfo
    {
        public Order[] Orders { get; set; }
    }
}
