﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StolovkaTest
{
    class Program
    {
        private static BlockingCollection<Food> _orders = new BlockingCollection<Food>();

        private static readonly string[] _food = new[] { "Meal", "Juice" };
        private static Random _rnd = new Random();
        private static int _currentTime = 0;

        static void Main(string[] args)
        {
            int NumOfTables = 5;

            for (int i = 0; i < NumOfTables; i++)
            {
                var t = new Thread(DoOrder);
                t.Name = $"Stolik {i}: ";
                t.Start();
            }

            Thread.Sleep(100);

            int numOfCoockers = 3;

            for (int i = 0; i < numOfCoockers; i++)
            {
                var x = new Thread(FinishOrder);
                x.Name = $"Povar {i}: ";
                x.Start();
            }
            
        }

        private static void DoOrder()
        {
            while (true)
            {
                Food tmp = new Food(_food[_rnd.Next(2)], _rnd.Next(10));
                _orders.Add(tmp);
                Console.WriteLine($"{Thread.CurrentThread.Name}{tmp}. CurTime: {_currentTime}");
                Thread.Sleep(8000);
            }
        }

        //Consumer
        private static void FinishOrder()
        {
            foreach (var v in _orders.GetConsumingEnumerable())
            {
                _currentTime += v.PreparingTime;
                Console.WriteLine($"{Thread.CurrentThread.Name}{v}. CurTime: {_currentTime}");
            }
        }
    }
}
