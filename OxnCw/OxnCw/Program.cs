﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OxnCw
{
    class Program
    {
        static void Main(string[] args)
        {
            OxnCows o = new OxnCows();
            o.ShowNumber();
           
            while (!o.IsGameOver())
            {
                string str = Console.ReadLine();
                if (!o.IsCorrectData(str))
                {
                    Console.WriteLine("Input Correct Number");
                    continue;
                }
                List<int> a = str.Select(i => int.Parse(i.ToString())).ToList();

                Tuple<int,int> t = o.CheckNumber(a);
                Console.Write($"{t.Item1} oxes AND {t.Item2} cows\n");
            }
            Console.Write(o.NumberOfTries());

            
        }
    }
}
