﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OxnCw
{
    class OxnCows
    {
        private List<int> _resNum;
        private int _counter = 0;

        private int _oxes = 0;

        public OxnCows()
        {
            GenerateNumber();
        }

        private void GenerateNumber()
        {
            _resNum = new List<int>();
            List<int> src = Enumerable.Range(0, 10).ToList();
            Random rnd = new Random();
            for (int i = 0; i < 4; i++)
            {
                int idx = rnd.Next(0,src.Count);
                _resNum.Add(src[idx]);
                src.RemoveAt(idx);
            }
        }

        public Tuple<int,int> CheckNumber(List<int> numbers)
        {
            int ox = 0;
            int cow = 0;

            _oxes = 0;

            for (int i = 0; i < numbers.Count; i++)
            {
                if (numbers[i] == _resNum[i])
                {
                    ox++;
                }
                else if (_resNum.Contains(numbers[i]))
                {
                    cow++;
                }

            }

            _oxes = ox;
            _counter++;
            return Tuple.Create(ox, cow);
        }

        public bool IsGameOver()
        {
            if (_oxes == 4) return true;
            return false;
        }

        public int NumberOfTries()
        {
            return _counter;
        }

        public bool IsCorrectData(string str)
        {
            Regex r  = new Regex(@"\d{4}");
            if (r.IsMatch(str) && str.Length == 4)
                return true;
            return false;
        }

        public void ShowNumber()
        {
            foreach (var i in _resNum)
            {
                Console.Write($"{i}");
                
            }
            Console.WriteLine();
        }
    }
}
