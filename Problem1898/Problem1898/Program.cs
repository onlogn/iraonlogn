﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace Problem1898
{
    class Program
    {
        static void Main(string[] args)
        {
            //БАГНУТАЯ ЗАДАЧА

            string s1 = Console.ReadLine();
            string s2 = Console.ReadLine();

            StringBuilder sb1 = new StringBuilder();
            for (int i = 0; i < s1.Length; i++)
            {
                if (Char.IsNumber(s1[i]) || s1[i] == '.')
                {
                    sb1.Append(s1[i]);
                }
            }

            StringBuilder cpf = new StringBuilder();
            for (int i = 0; i < sb1.Length && i < 11; i++)
            {
                if (sb1[i] == '.')
                    break;
                cpf.Append(sb1[i]);
            }

            Console.WriteLine($"cpf {cpf.ToString()}");

            StringBuilder n1 = new StringBuilder();
            for (int i = cpf.Length; i < sb1.Length; i++)
            {
                n1.Append(sb1[i]);
            }

            double a1 = Math.Floor(double.Parse(n1.ToString()) * 100) * 0.01;

            //Console.WriteLine($"{a1}");


            StringBuilder n2 = new StringBuilder();
            for (int i = 0; i < s2.Length; i++)
            {
                if (Char.IsNumber(s2[i]) || s2[i] == '.')
                {
                    n2.Append(s2[i]);
                }
            }

            double a2 = Math.Floor(double.Parse(n2.ToString()) * 100) * 0.01;
            //Console.WriteLine(a2);

            Console.WriteLine($"{(a1+a2):f2}");

            //var res1 = s1.Where(i => Char.IsNumber(i) || i=='.');
            //var cpf = res1.Take(11);

            //Console.WriteLine($"cpf {cpf.IenumerableToString()}");

            //var first = res1.Skip(11);
            //string a1 = String.Join("", first);
            //double f = double.Parse(a1);
            //double x1 = Math.Floor(f * 100) * 0.01;

            //var second = s2.Where(i => Char.IsNumber(i) || i == '.');
            //a1 = String.Join("", second);
            //f = double.Parse(a1);
            //double x2 = Math.Floor(f * 100) * 0.01;

            //Console.WriteLine($"{(x1 + x2):f2}");




        }


    }

    public static class StringExtension
    {
        public static string IenumerableToString(this IEnumerable<char> s)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var x in s)
            {
                sb.Append(x);
            }

            return sb.ToString();
        }
    }
}
