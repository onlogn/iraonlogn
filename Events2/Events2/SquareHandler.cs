﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events2
{
    public class SquareHandler
    {
        public void Show(object o, NumberChangedEventArgs a)
        {
            Console.WriteLine($"{a.Number * a.Number}");
        }
    }
}
