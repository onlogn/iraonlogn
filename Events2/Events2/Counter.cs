﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Events2
{
    public class Counter
    {
        public event EventHandler<NumberChangedEventArgs> NumberChanged;

        public void Count()
        {
            for (int i = 0; i <= 100; i++)
            {
                NumberChangedEventArgs n = new NumberChangedEventArgs(){Number = i};
                Console.WriteLine(i);
                OnNumberChanged(n);
                Thread.Sleep(100); 
            }
        }

        protected virtual void OnNumberChanged(NumberChangedEventArgs e)
        {
            NumberChanged?.Invoke(this, e);
        }
    }
}
