﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events2
{
    public class OddHandler
    {
        public void Show(object o, NumberChangedEventArgs a)
        {
            string str = a.Number % 2 == 0 ? "Чет" : "Нечет";
            Console.WriteLine($"{a.Number} is {str}, {o}");
        }
    }
}
