﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events2
{
    class Program
    {
        static void Main(string[] args)
        {
            Counter c = new Counter();
            OddHandler h = new OddHandler();
            SquareHandler s = new SquareHandler();

            c.NumberChanged += h.Show;
            c.NumberChanged += s.Show;

            c.Count();

        }
    }
}
