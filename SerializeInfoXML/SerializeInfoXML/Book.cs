﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace SerializeInfo1
{
    [Serializable]
    public class Book
    {
        public int Id;
        public string Title;
        public string Author;
        public int Year;

        public Book()
        {
            
        }

        public Book(int id, string title, string author, int year)
        {
            Id = id;
            Title = title;
            Author = author;
            Year = year;
        }

        public Book(Book b) : this(b.Id, b.Title, b.Author, b.Year)
        {

        }

        private bool Equals(Book a)
        {
            if (ReferenceEquals(null, a)) return false;
            if (ReferenceEquals(this, a)) return true;
            return Equals(a.Id, Id) && Equals(a.Title, Title) && Equals(a.Author, Author) && Equals(a.Year, Year);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Book)) return false;
            return Equals((Book)obj);
        }

        public override int GetHashCode()
        {
            int num = 13;

            var res = Id.GetHashCode();
            res = res * num + Title.GetHashCode();
            res = res * num + Author.GetHashCode();
            res = res * num + Year.GetHashCode();

            return res;
        }
    }
}
