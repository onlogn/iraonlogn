﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using SerializeInfo1;

namespace SerializeInfoXML
{
    class Program
    {
        static void Main(string[] args)
        {
            string file = "file.txt";

            List<Book> books = new List<Book>();
            books.Add(new Book(1,"hz","hh",2001));
            books.Add(new Book(2,"hz2","hh2",2002));

            XmlSerializer xml = new XmlSerializer(typeof(List<Book>));

            using (FileStream fs = new FileStream(file,FileMode.OpenOrCreate))
            {
                xml.Serialize(fs,books);
            }

            List<Book> res = new List<Book>();
            using (FileStream fs = new FileStream(file, FileMode.Open))
            {
                res = (List<Book>)xml.Deserialize(fs);

                foreach (var v in res)
                {
                    Console.WriteLine($"{v.Id}, {v.Title}, {v.Author}, {v.Year}");
                }
            }

        }
    }
}
